#!/bin/bash
if [ $#  -lt 1 ];then
    echo "$0, file_name"
    exit
fi
file_=$1
#pdflatex -synctex=1  $file_.tex
pdflatex  $file_.tex

## run feynman diagram
for mp_ in *.mp
do
    echo $mp_
    #mpost --interaction nonstopmode  $mp_
done

bibtex $file_.aux
#pdflatex -synctex=1 -interaction=nonstopmode $file_.tex
#pdflatex -interaction=nonstopmode $file_.tex
pdflatex $file_.tex
