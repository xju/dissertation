% Reconstruction and identification of physics objects
\clearpage
\chapter{Object Reconstruction and Identification}
\label{ch:objects}
The four-lepton channel has a small rate but is a relatively clean final state
where the signal-to-background ratio, taking into reducible backgrounds into account alone,
i.e. ignoring the \ZZ background, is above 6 in the observation of a SM Higgs boson analysis;
the search for additional heavy scalars in high-mass regions is almost background free.
Significant effort was made to obtain a high efficiency for the reconstruction
and identification of electrons and muons, while
keeping the loss due to background rejection as small as possible.
In particular, this becomes increasingly difficult for electrons as \et decreases.

Electrons are reconstructed using information from the ID and the electromagnetic
calorimeter. For electrons, background discrimination relies
on the shower shape information available from the highly segmented LAr 
EM calorimeter, high-threshold TRT hits, as well as compatibility of the tracking
and calorimeter information.
Muons are reconstructed as tracks in the ID and MS, and their identification
is primarily based on the presence of a matching track or tag in the MS.
Finally, jets are reconstructed from clusters of calorimeter cells and calibrated
using a dedicated scheme designed to adjust the energy measured in the 
calorimeter to that of the true jet energy on average.

\section{Electron reconstruction and identification}
\label{sub:electron}

\paragraph{Electron Reconstruction} 
Electron reconstruction in the central region of the  ATLAS detector ($|\eta| < 2.47$) 
starts from energy deposits (seed-clusters) in the EM calorimeter that are matched to 
reconstructed tracks of charged particles in the inner detector.
The $\eta$--$\phi$ space of the EM calorimeter system is divided into a grid
of $N_\eta \times N_\phi = 200 \times 256$ towers of size 
$\Delta\eta^{\text{tower}} \times \Delta\phi^{\text{tower}} = 0.025 \times 0.025$,
corresponding to the granularity of the EM accordion calorimeter middle layer.
The energy of the calorimeter cells in all longitudinal-depth layers is summed 
to get the tower energy.
Seed clusters of towers with total transverse energy above 2.5~\gev\ 
are searched for by a sliding-window algorithm.
For each seed cluster passing loose shower-shape requirement, 
a region-of-interest is defined as a cone of size $\Delta R = 0.3$ around the seed cluster barycenter.
An electron is reconstructed if at least one track is matched to the seed cluster.
To improve reconstruction efficiency for electrons that undergo significant energy loss 
due to bremsstrahlung,
the track associated to seed cluster is refitted using a Gaussian-Sum Filter~\cite{EgammaPaper}.
The electron reconstruction efficiency is 97\% for electrons with $\et = 15$~\gev\ and 
99\% at $\et = 50$~\gev. 

\paragraph{Electron Identification} 
Not all objects built by the electron reconstruction algorithms are 
prompt electrons, which are referred to those coming from $W$ or $Z$-boson decays.
Background objects include hadronic jets as well as the electrons from converted photon decays, 
Dalitz decays and semi-leptonic heavy-flavor hadron decays.
To distinguish signal electrons from background objects,
the variables that describe longitudinal and lateral shapes of the electromagnetic 
showers in the calorimeters,
the properties of the tracks in the ID and 
the matching between tracks and energy clusters are used in a multivariate technique.
Out of different multivariate techniques, the likelihood  was chosen for
electron identification because of its simple construction.
An overall probability for each object to be signal or background is calculated as:
$$d_L = \frac{L_S}{L_S + L_B}, \; L_{S(B)} (\vec{x}) = \prod_{i=1}^{n} P_{S(B),i} (x_i)$$
where $\vec{x}$ is the vector of variable values
and $P_{S,i}(x_i)$ is the value of the signal probability density function (PDF) of the $i_{th}$ variable
evaluated at $x_i$. In the same way, $P_{B,i}(x_i)$ refers to that for the background.
These signal and background PDFs are obtained from data in different $|\eta|$ and \et bins.
%This discriminant uses probability density model for real electrons and fakes, 
%which are obtained from simulation for both 2015 and 2016 data \cite{ElID2015}, 
%and combines them into a single score, namely Likelihood (LH).
By applying a cut on the $d_{L}$ to match the target signal efficiency expectations, 
different working points are defined, including ``Loose'', ``Medium'' and ``Tight'', 
The ``Loose'' criteria is used in this analysis as well as that an insertable b-layer hit or, if no such hit is expected, an innermost pixel hit is required for all electrons.
The signal efficiency, including reconstruction and identification, is measured both in data and MC simulation\cite{ElIDMoriond2016} as a function of the transverse energy \et and the pseudo-rapidity $\eta$  as shown in Figure~\ref{fig:el_eff}.
%https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/ATLAS-CONF-2016-024/#figures
\begin{figure}
\includegraphics[width=0.49\linewidth]{figures/cp/el_eff_et}
\includegraphics[width=0.49\linewidth]{figures/cp/el_eff_eta}
\caption{Combined electron reconstruction and identification efficiencies in $Z\to ee$ events as a function of the transverse energy \et, integrated over the full pseudo-rapidity range (left), and as a function of pseudorapidity $\eta$, integrated over the full ET range (right). The data efficiencies are obtained from the data-to-MC efficiency ratios measured using \Jpsi and Z tag-and-probe, multiplied by the MC prediction for electrons from $Z \to ee$ decays. The uncertainties are obtained with pseudo-experiments, treating the statistical uncertainties from the different (\et, $\eta$) bins as uncorrelated. Two sets of uncertainties are shown: the inner error bars show the statistical uncertainty, the outer error bars show the combined statistical and systematic uncertainty.
\label{fig:el_eff}}
\end{figure}
The disagreements between efficiencies obtained in data and the simulated events are around 5\%, due to
the known mis-modeling of shower shapes and other identification variables in the simulation.
The ratios of the efficiencies measured in data and simulation (so called ``scale factors'') and 
their associated uncertainties are used in this analysis to correct the yields of electrons in simulated events.
For electrons with $\et < 30$ \gev, the total uncertainties on the efficiencies vary from 1\% to 3.5\%, dominated by statistic uncertainties. For electrons with $\et > 30$ \gev, the total uncertainties are below 1\%.


\section{Muon reconstruction and identification}
\label{sub:muons}
\paragraph{Muon Reconstruction}
Various algorithms are used to reconstruct muons by combining the information from the 
muon spectrum (MS), inner detector (ID) and calorimeter \cite{PERF-2014-05, PERF-2015-10}. 
This results in five different types of muons:
\begin{itemize}
\item \textbf{Combined muons} (CB): Starting from tracks reconstructed in the MS and extrapolating
    to the interaction point (ME track), it searches for an inner detector (ID) track within 
    a cone around 
    the ME track. Then performs a combined fit using the hits of the ID track, the energy 
    loss in the carometer and the hits of the ME track. A complementary algorithm is designed 
    to improve the efficiency when no good ME tracks are reconstructed. This algorithm starts
    from an ID track with \pt $>$ 2~\gev\ and projects outwards using a neural network to
    select reconstructed segments (basic elements for reconstructing ME tracks). These 
    segments are then fit together with the ID track using a similar procedure to that 
    in CB case. The muons reconstructed from this algorithm is named as \textit{MuGirl}.
\item \textbf{Segment tagged muons} (ST): A track in the ID is identified as a muon if the 
    trajectory extrapolated to the MS can be associated with  track segments in the 
        precision muon chambers. 
        %ST muons adopt the measured parameters of the associated ID track. 
\item \textbf{Calorimeter tagged muons} (CT): A trajectory in the ID is identified as a muon if the associated energy deposits in the calorimeters are compatible with the hypothesis of a minimum ionized particle. It's used to recover the efficiency in the region of $|\eta| < 0.1$ which is not equipped with muon chambers. The calorimeter muon identification is optimized for muons with $\pt > 15 \gev$.
\item \textbf{Silicon-associated forward muons} (SiAF): 
In the $2.5 < |\eta| < 2.7$ region where ID provides little coverage, tracks are still reconstructed in the MS and extrapolated back to interaction point. If a very forward ID tracklet formed by Silicon hits only (no TRT hits) is found within a cone around the ME track, a combined fit is performed including the hits of the ID tracklet. 
\item \textbf{Standalone muons} (SA): Any ME track that is successfully extrapolated back to the interaction point and refitted with a loose interaction point constraint, taking into account the energy loss in the calorimeter, but is not matched to an ID track or forward tracklet, is reconstructed as a standalone muon.
\end{itemize}

\paragraph{Muon Identification}
Muon identification is performed by applying a set of quality requirements 
based on the muon types described above. 
The fake muon mainly comes from pion and kaon decays. 
The CT and ST muons are restricted to the $|\eta| < 0.1$ region 
while SiAF and SA muons are allowed only in $2.5 < |\eta| < 2.7$. A set of track quality requirements is applied to the ID tracks to reject poorly reconstructed charged-particle trajectories. 
Different MS hit requirements are applied to different muon types. 
SA and SiAF muons are required to have $\ge 3$ precision hits (i.e. MDT or CSC hits)
 in each of the three layers of the MS.
Combined muons are required to have $\ge 3$ precision hits in at least two layers of MDT, except for $|\eta| < 0.1$ region where tracks with at least three hits in one single MDT layer are allowed. 
To suppress muons produced from in-flight decays of hadrons, 
the difference of measured charge-over-momentum between the ID and the MS
is required to be small:
$$\frac{|q/p_{\text{ID}} - q/p_{\text{MS}}|}{\sqrt{\sigma_{\text{ID}}^2 + \sigma_{\text{ME}}^2}} < 7$$
For muons with $|\eta| < 2.7$ and $5 < \pt < 100~\gev$,
the reconstruction and identification efficiency is above 99\% for 
\RunOne data set~\cite{PERF-2014-05}
and close to 99\% for \RunTwo data set~\cite{PERF-2015-10}.

\section{Jet reconstruction}
\label{sub:jets}
Jets are reconstructed at the electromagnetic energy scale (EM scale)
with the \antikt algorithm~\cite{Cacciari:2008gp} and radius
parameter $R = 0.4$ using the \textsc{FastJet} software package~\cite{Cacciari:2011ma}.
A collection of three-dimensional, massless, positive-energy topological
clusters (topo-clusters)~\cite{Aad:2016upy, Aad:2011he} made of calorimeter cell
energies are used as input to the \antikt algorithm.
Topo-clusters are built from neighboring calorimeter cells containing a significant energy
above a noise threshold that is estimated from measurements of calorimeter electronic
noise and simulated pileup noise.
The topo-cluster reconstruction algorithm was improved in 2015, specially preventing 
topo-clusters from being seeded by the pre-sampler layers.
%which are in front of the LAr EM calorimeter. 
This restricts jet formation from low-energy pileup depositions that do not penetrate the 
calorimeters~\cite{Aaboud:2017jcu}. 

Ref.~\cite{Aaboud:2017jcu} details the methods used to calibrate the four-momentum of jets
in Monte Carlo simulation and in data collected by the ATLAS detector at a center-of-mass
energy of $\sqs = 13~\tev$. The jet energy scale (JES) calibration consists of 
several consecutive stages derived from a combination of MC-based methods and \textit{in situ}
techniques. MC-based calibrations correct the reconstructed jet four-momentum to that 
found from the simulated stable particles within the jet.
The calibrations account for features of the detector, the jet reconstruction algorithm,
jet fragmentation, and the busy data-taking environment resulting from multiple $pp$
interactions, referred to as pileup.
\textit{In situ} techniques are used to measure the difference in jet response between data and
MC simulation, with residual corrections applied to jets in data only.
The jet calibration in Run 2 builds on procedures developed for the 2011 data~\cite{Aad:2014bia}
collected at $\sqs = 7~\tev$ during Run 1. Aspects of the jet calibration,
particularly those related to pileup~\cite{Aad:2015ina}, were also developed on 2012
data collected at $\sqs = 7~\tev$ during Run 1.

Fake jets are mainly from beam-induced non-$pp$ collision events~\cite{Aad:2013zwa}, 
%due to proton collisions upstream of the interaction point, 
cosmic-ray showers produced in the atmosphere overlapping with collision event, 
and calorimeter noise from large scale coherent noise or isolated pathological cells;
therefore, \textit{loose} jet quality criteria are applied.
The \textit{loose} jet quality criteria is designed to provide an efficiency of 
selecting jets from $pp$ collisions above $99.5\%$ (99.9\%) for $\pt > 20 (100)$~\gev,
as described in Ref.~\cite{jetcleaning:2015}.
%which are based on the variables described in Ref.~\cite{jetcleaning:2015},  
In the other hand, pileup jets, resulting from multiple $pp$ interactions, are 
real jets but of no physics interest.
They are suppressed by using jet-vertex-tagger (JVT)~\cite{jvttagger:2014}.
JVT is a likelihood ratio that is based on two variables: the corrected jet-vertex fraction, which is
the fraction of the total momentum of the tracks inside the jets over those associated with
the primary vertex; 
and $R_{\pt}$, which is defined as the scalar \pt sum of the tracks that are associated with
the jet and originate from the hard-scatter vertex divided by the fully calibrated jet \pt.
The efficiency of the JVT  selecting non-pileup jets is about 97\%.
% https://twiki.cern.ch/twiki/bin/view/AtlasProtected/JVTCalibration
