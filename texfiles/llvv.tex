%% description of llvv analysis.
\subsection{Search for heavy resonances in \llvv final state}
\label{sec:llvv}
The search for heavy resonances in the \llvv final state
is to select the events that contain two oppositely charged
isolated leptons that are originated from a on-shell Z boson
along with a large missing transverse momemtum (\met), which
is preassumably from the neutrinos that are decayed 
from the other Z boson.
This search lookes for an excess in the transverse mass spectrum \mt, 
defined as:
\begin{equation}
  \mt \equiv \sqrt{ \left[ {\sqrt{\mz^{2} + \left(\ptll\right)^2} + \sqrt{\mz^{2} + \left( \met \right)^2}}\: \right]^2 - \left\lvert \vecptll  + \vecmet \right\rvert^2 }
\label{eq::mt}
\end{equation}
where \mz is the pole mass of the Z boson, \ptll is 
the transverse momentum of the lepton pair, \vecmet
is the missing transverse momentum and \met is the 
mangnitude of \vecmet.
The \llvv final state posses larger branching ratio than the \llll
final state and better resolution than the \vvqq final state.
Combination of the \llll and \llvv enhances the search sensitivies 
over the whole mass range, as \llll is sensitive to the low mass
region while \llvv dominates in the high mass region.

\paragraph{Event Selections}
\label{subsec:selllvv}
The event selections are designed to discriminate against the 
\Zjets, \WZ and top-quark backgrounds. Events are required to pass
either a single electron or muon trigger, where different \pT thresholds
are used depending on the instantaneous luminosity of the LHC.
The trigger efficiency for signal events passing the final 
selection is about 99\%.
Selected events must have exactly two opposite-charge leptons of
the same flavor and the ``medium'' identification, with 
the more energetic lepton having $\pt > 30~\gev$\ and the other one
having $\pt > 20~\gev$.
The same impact parameter significance criteria as defined in Chapter~\ref{ch:sel}
are applied to the selected leptons.
Track- and calorimeter-based isolation criteria as defined in Chapter~\ref{ch:sel}
are also applied to the leptons, but in this analysis
the criteria are optimised such that by adjusting the isolation threshold
theselection efficiency of the isolation criteria is 99\% for
signal leptons.
If an additional lepton with $\pt > 7~\gev$\ and ``loose''
identification is found then the event is rejected, to reduce the
amount of the \WZ background.
In order to select leptons originating from the decay of a Z boson,
the invariant mass of the lepton pair is required to be in the 
range of 76 to 106~\gev.
Moreover, since a Z boson originating from the decay of a high-mass
particle will be boosted, the two leptons are required to be
produced with a small angular separation $\Delta R_{\ell\ell} < 1.8$.

Events with neutrinos in the final state are selected by 
imposing $\met > 120~\gev$, and this requirement heavily reduces the 
amount of \Zjets background. In signal events with no initial-
or final-state radiation the Z boson is expected to be produced
back-to-back with respect to the missing transverse momemtum,
and this characteristic is used to further suppress the \Zjets background.
The azimuthal angle between dilepton system and the missing
transverse momentum ($\Delta\Phi(\ell\ell, \vecmet)$) is thus required to
be greater than 2.7 and the fractional \pt difference, defined as
$|p_\text{T}^{\text{miss,jet}} - \ptll|/\ptll$, to be less than 20\%,
where $p_\text{T}^{\text{miss,jet}}  = |\vecmet + \sum_{\text{jet}}\vec{\pt}^{\text{jet}}|$.

Additional selection criteria are applied to keep only events with
\met originating from neutrinos rather than detector inefficiencies,
poorly reconstructed high-\pt muons or mis-measurements in the
hadronic calorimeter. If at least one reconstucted jet has a \pt
greater than 100~\gev, the azimuthal angle between the highest-\pt
jet and the missing transverse momentum is required to be greater than
0.4. 
Similarly, if \met is found to be less than 40\% of the scalar sum 
of the transverse momenta of leptons and jets in the event ($H_T$),
the event is rejected. 
Finally, to reduce the \ttbar background, events are rejected whenever
a b-jet is found.

The sensitivity of the analysis to the VBF production mode is increased
by creating a dedicated category of VBF-enriched events.
An optimisation procedure based on the significance obtained by 
using signal and background MC samples is performed and the
selection criteria require the presence of at least two jets with
$\pt > 30~\gev$\ checking that the two highest-\pt jets are widely
separated in $\eta$, $|\Delta\eta_{jj}| > 4.4$, and have
a invariant masss $m_{jj}$ greater than 500~\gev.

The signal acceptance, defined as the ratio of the number of reconstructed
events passing the analysis requirements to the number of 
simulated events in each category, for the \llvv analysis is shown in
Table~\ref{tab:H2l2vacc}, for the ggF and VBF production modes as well as for
different resonance masses. 
\input{texfiles/hllvv_acceptance}

The \llvv search starts only from 300~\gev because this is where it
begins to improve the combined sensitivity as the acceptance increases
due to a kinematic threshold coming from the \met selection criteria,
also seen from Table~\ref{tab:H2l2vacc}.

\paragraph{Background Estimation}
\label{sec:sigbkgllvv}
The dominant and irreduciable background for this search is the 
non-resonant \ZZ production which accounts for about 60\% of the expected
background events.
The second largest background comes from the \WZ production ($\sim$30\%)
followed by \Zjets production with poorly measured jets ($\sim$6\%).
Other sources of background are the \WW, \ttbar, \Wt and \Ztt processes ($\sim$3\%).
Finally, a small contribution comes from \Wjets, single-top quark 
and multi-jet processes, with at least one jet mis-identified as
an electron or muon, as well as from $\ttbar V/VVV$ events.

The \ZZ production is modeled with MC simulation and normalised to 
SM predictions, as explained in Section~\ref{sec:mc}.
The remaining backgrounds are mostly estimated by extrapolating 
the events in a control region to the signal region using dedicated 
transfer factors.

The \WZ background is modeled by MC simulation and a correction factor
for its normalization is extracted as the ratio of data events to
the simulated events in a \WZ-enriched control region, after subtracting
from data the non-\WZ background contribution.
The \WZ-enriched control region, called the $3\ell$ control region,
is built by selection $Z\to \ell\ell$ candidates with an additional
electron or muon. This additional lepton is required to pass all
selection criteria used for the other two leptons, with the only 
difference that its transverse momentum is required to be greater than
7~\gev.
The contamination from \Zjets and \ttbar events is reduced by 
vetoing events with at least one reconstructed b-jet and
by requiring the transverse mass of $W$ boson ($m_{\text{T}}^{\text{W}}$),
built using the additional lepton and the \vecmet, 
to be greater than 60~\gev.
The distribution of the missing transverse momentum for data
and simulated events in the $3\ell$ control region is shown
in Figure~\ref{fig:llvv_crs_sub1}.
The correction factor derived in the $3\ell$ control region is found to be 
$1.29\pm0.09$, where the uncertainty includes effects from the 
statistics of the control region as well as from experimental 
systematic uncertainties.
Due to poor statistics when applying all the VBF selection requirements
to the WZ enriched control sample, the estimate for the VBF-enriched
category is performed by including in the $3\ell$ control region only
the requirement of at least two jets with $\pt > 30~\gev$.
Finally, a transfer factor is derived from MC simulation by calculating the
probability of events passing all analysis selections and containing 
two jets with $\pt > 30~\gev$ to satisfy the VBF selections

The non-resonant background includes mainly \WW, \ttbar and \Wt processes,
but also \Ztt events in which the $\tau$ leptons produce light
leptons and \met. It is estimated by using a control sample of
events with lepton pairs of different flavour ($\emu$), passing
all analysis selection criteria.
Figure~\ref{fig:llvv_crs_sub2} shows the missing transverse momentum
distribution for \emu evens in data and simulation after applying
the dilepton invariant mass selection but before applying the 
other selection requirements.
The non-resonant background in the \ee and \mm channnels is estimated
by applying a scale factor ($f$) to the events in the $\emu$ 
control region, such that:
\begin{equation}
  \neebkg = \frac{1}{2} \times \nemu \times f, 
  \hspace{1cm} 
  \nmmbkg = \frac{1}{2} \times \nemu \times \frac{1}{f} ,
\label{eq::emu_cr}
\end{equation}
where \neebkg and \nmmbkg are the numbers of electron
and muon pair events estimated in the signal region 
and \nemu is the number of events in the \emu control 
sample with \ZZ, \WZ and other small backgrounds
subtracted using simulation. The factor $f$ takes
into account the different selection efficiency
of \ee and \mm pairs at the level of the \Zll
selection, and is measured from data as
$f^2 = \needata/\nmmdata$, where \needata
and \nmmdata are the number of events passing
the $Z$ boson mass requirement ($76 < \mll < 106~\gev$)
in the electron and muon channel respectively.
As no events survivein the \emu control region after
applying the full VBF selections,
the background estimate is performed by including
only the requirement of at least two jets with $\pt > 30~\gev$.
The efficiency of the remaining selection criteria on 
\deta and \mjj is obtained from simulated events.

\begin{figure}[tbp]
  \subfigure[\label{fig:llvv_crs_sub1}]{\includegraphics[width=0.49\textwidth]{figures/plot_3lcr_2l2v}}
  \subfigure[\label{fig:llvv_crs_sub2}]{\includegraphics[width=0.49\textwidth]{figures/plot_emucr_2l2v}}
  \caption{Missing transverse momentum distribution \subref{fig:llvv_crs_sub1} for events in the \threel control region as defined in the text and \subref{fig:llvv_crs_sub2} for \emu lepton pairs after applying the dilepton invariant mass selection. The backgrounds are determined following the description in Section~\ref{sec:sigbkgllvv} and the last bin includes the overflow. The error bars on the data points indicate the statistical uncertainty, while the systematic uncertainty on the prediction is shown by the \systband\ band. The bottom part of the figures shows the ratio of data over expectation. }   
\label{fig:llvv_crs}
\end{figure}


The number of \Zjets background events in the signal region is estimated from data, 
using a so-called ABCD method~\cite{STDM-2012-07}, since events with no genuine \met 
in the final state are difficult to model using simulation.
The method combines the selection requirements presented in 
Section~\ref{subsec:selllvv} (with $n_{b-\textrm{jets}}$ representing the 
number of $b$-jets in the event) into two Boolean discriminants, 
\varone and \vartwo, defined as:

\begin{gather}
  \varone \equiv \met > 120~\gev \andand \met / \HT > 0.4, \\ 
  \vartwo \equiv \fracpt < 0.2 \andand \Delta\phi(\ell\ell,\vecmet) > 2.7 \andand \Delta R_{\ell\ell} < 1.8 \andand n_{b-\textrm{jets}} = 0,
\label{eq::abcd}
\end{gather}
with all events required to pass the trigger and dilepton invariant mass selections. 
The signal region (A) is thus obtained by requiring both \varone and \vartwo to be true, 
control regions B and C require only one of the two booleans to be 
false (\varone and \vartwo respectively) and 
finally control region D is defined by requesting both \varone and \vartwo to be false.
With this definition, %assuming no correlation between \varone and \vartwo, 
an estimate of the number of events in region A is given 
by $\nnn{A}{est} = \nnn{C}{obs} \times (\nnn{B}{obs}/\nnn{D}{obs})$, where \nnn{X}{obs}
is the number of events observed in region X after subtracting non-$Z$-boson backgrounds.
This relation holds as long as the correlation between \varone and \vartwo is small, 
and this is obtained by introducing two additional requirements 
on control regions B and D, namely \met $>$ 30~\gev\ and $\met / \HT > 0.1$.
The estimation of the \Zjets background was cross-checked with another 
approach in which a control region is defined 
by inverting the analysis selection on $\met / \HT$ and then 
using \Zjets\ Monte Carlo simulation 
to perform the extrapolation to the signal region, yielding results 
compatible with the ABCD method. 
Finally, the estimate for the VBF-enriched category is performed by 
extrapolating the inclusive result obtained 
with the ABCD method to the VBF signal region, extracting the 
efficiency of the two-jet, \deta and \mjj selection criteria 
from \Zjets\ simulation.

The \Wjets and multi-jet backgrounds are estimated from data 
using a so-called fake factor method~\cite{STDM-2011-16}.
A control region enriched in fake leptons is designed by requiring 
one lepton to pass all analysis requirements (baseline selection) 
and the other one to fail either the lepton ``medium'' 
identification or the isolation criteria (inverted selection).
The background in the signal region is then derived using a 
transfer factor, measured in a data sample enriched in \Zjets\ events,
as the ratio of jets passing the baseline selection to those passing the inverted selection.

Finally, the background from the \ttv and \vvv processes is estimated using MC simulation.

\input{texfiles/hllvv_modeling}

\paragraph{Results}
Table~\ref{tab:ZZllnn_yields_postfit} contains the number of observed candidate events along with
the background yields for the $\llvv$ analysis, while Figure~\ref{fig:llvv_sig} shows the \mt
distribution for the electron and muon channels with the ggF-enriched and VBF-enriched categories 
combined.
\input{texfiles/hllvv_yields}

\begin{figure}[tbp]
  \subfigure[\label{fig:llvv_sig_sub1}]{\includegraphics[width=0.49\textwidth]{figures/plot_sr_eevv}}
  \subfigure[\label{fig:llvv_sig_sub2}]{\includegraphics[width=0.49\textwidth]{figures/plot_sr_mmvv}}
  \caption{Transverse invariant mass distribution in the $\llvv$ search for \subref{fig:llvv_sig_sub1} the electron channel and \subref{fig:llvv_sig_sub2} the muon channel, including events from both the ggF-enriched and the VBF-enriched categories. The backgrounds are determined following the description in Section~\ref{sec:sigbkgllvv} and the last bin includes the overflow. The error bars on the data points indicate the statistical uncertainty and markers are drawn at the bin centre. The systematic uncertainty on the prediction is shown by the \systband\ band. The bottom part of the figures shows the ratio of data over expectation.  }  \label{fig:llvv_sig}
\end{figure}
