\newpage
\section{\llee background} 

\label{sec:bkg_zee}  

The estimation of the electron background is extracted from a control region denoted as 
$3\ell$+$X$, where the selection and identification criteria for the low $\pt$ 
electron in the subleading pair (denoted with ``$X$'') are relaxed.
%Although the use of this CR poses statistical limitations with respect to the
%alternative method used in Run-I where the criteria for both electrons of the
%subleading pair are relaxed (``$Z$+$XX$'' method), it has the advantage
%of lower systematics. Therefore the ``$3\ell$+$X$'' method is chosen as the
%baseline and is detailed in \refS{sec:z3plus1}.
%In the $3\ell$+$X$ method, the estimate of the electron background is
%obtained using a control region with three electrons passing the full
%analysis selection, while the lepton candidate that completes the quadruplet
%is only passing loose requirements.
%This approach simplifies significantly the decomposition of the different background contributions as it can be done by looking only at the lowest $\pt$ lepton.
The electron background is classified according to the process involved.
The major contribution is coming from light jets with depositions in
the calorimeter faking an electron ($f$).
Also important are electrons coming from photon conversions or FSR ($\gamma$).
Electrons from semileptonic decays of heavy quarks ($q$) also contribute a
considerable amount.
Each background has different properties and efficiency, therefore
the background estimation method is targeted to disentangle
the various components using suitable discriminating variables.
In MC simulation, the actual origin is known and is extracted using
truth information (\texttt{MCTruthClassifier}).
For data, the various components are unfolded directly using a template
fit on the number of pixel hits,\nInnerPix.
This variable counts the number of IBL hits, except if no such hits are expected due to a dead area of the IBL: in such cases, the number of hits on the next-to-innermost pixel layer is counted instead.
It provides discrimination for $\gamma$ over $f$ and $q$, as photons populate $\nInnerPix=0$ in the distribution.
%This replaces the \nBL observable used in previous iterations of the analysis, which simply counted the number of IBL hits.
The new observable recovers the ability to reject photon background in the dead-module regions, reducing the fraction of events that originate from regions where no hit is expected from a few percent to the per-mille level.
%The TRT-based observables that have been used in previous iterations of the analysis (\probHT, \rTRT) have been abandoned because of their poor description in the simulation which uses an inconsistent gas mixture with respect to the data conditions for the TRT.
%Moreover, these observables were effective only for $|\eta|<2$ due to the coverage of the TRT, requiring an additional correction factor of about 1.2 to scale the estimated backgrounds to the full $\eta$ range; this is no longer required.
%Differences in data-taking conditions between 2015 and 2016 could potentially affect the method: this possibility was studied in a previous note [reference the ICHEP note] and no effects were found, so it is neglected here.

%A complementary control region denoted as $Z$+$X$ is used to estimate
%the efficiency needed to extrapolate from the relaxed
%electron requirements of ``$X$'' for each of the background components
%to the full electron identification and the isolation cuts used in the signal region.
%Details are given in \refS{subsec:ZplusEl}.
%\subsection{Control region with $3\ell$+$X$}
%\label{sec:z3plus1}

%Estimating the electron background in a control region with three leptons passing the full selection
%has the advantage that one has to deal only with the background
%composition of the fourth lepton that completes the quad. 
In the $3\ell$+$X$ CR, the first three leptons of the quadruplet are obtained as for the full analysis,
with all the cuts applied, while the
lowest \et electron in the subleading lepton pair is only required to pass
relaxed identification criteria.
In particular, a modified \emph{Loose} working point is used in which
the likelihood identification cut and the \nInnerPix requirement
are removed so that only track quality cuts are applied. The $4\ell$ vertex cut is applied, but neither the lowest \et electron isolation selection nor $d_0$-significance are.
Additionally, to suppress the $ZZ^*$ contribution only same-sign dileptons are
considered in the subleading pair.
However, even after this same-sign selection, $5\%$ of the remaining events come from $ZZ^*$ and
this contribution is subtracted from the final estimate based on MC simulation.
Finally, all quadruplets sharing the same $m_{12}$ in the event are
considered, as the probability of each one to contribute to the
background in the signal region is evaluated.

The distribution of \nInnerPix for events falling in this control region is fitted
to templates which characterize the $\gamma$ and $f$ backgrounds in order to
extract the yields of each component. The templates are obtained from MC
simulation and in particular from the $Z$+$X$ CR, as there are not enough statistics in the MC sample
for the $3\ell$+$X$ CR. Small corrections are applied to the templates for $\gamma$ and $f$ in
order to describe the data better, although they are found to have an insignificant effect on the 
fit result (2\% for $\gamma$ and $<1$\% for $f$). 
%\refS{subsec:ZplusEl} provides more details regarding the
%extraction of the templates and their corrections.
%As mentioned above, although greatly suppressed $ZZ^*$ still has a small contribution in both $f$ and $\gamma$, mostly due to fakes replacing the fourth true
%electron when it falls outside the event selection.
%To account for these fakes, $f$ and $\gamma$ contributions from $ZZ^*$ are subtracted
%from the final fit results according to the expectations from MC simulation.
The contribution of heavy-flavour electrons in the $3\ell$+$X$ CR is $\sim1$\% and 
is not considered as a separate template in the fit. The expected contribution is 
subtracted from the fit result. Other processes that may contribute with a real 
electron are mostly $ZZ^*$ with charge flip for one electron so that the subleading 
pair is of same-sign. The expected contribution is below 1\% and can be safely neglected.

 
\paragraph*{Results of the fit to data} The fit on \nInnerPix
 distribution described above is performed on data combining the
$2\mu 2e$ and $4e$ channels.
The distributions of the data and the fit result in the $3\ell$+$X$ CR are
shown in \refF{fig:ThreePlusOneFit}.
The fit is also performed using the templates without corrections; the
effect is significant only for $\gamma$ (2\%) and is added to the total 
systematic uncertainty.

 \begin{figure}[!htbp]
  \centering
  \subfigure[]{\includegraphics[width=0.45\textwidth]{figures/reducible_bkg/Fit_relaxee_nInner_data_LOG}} 
 \caption{Data $3\ell$+$X$ events and result of the fit to \nInnerPix, combining $2\mu 2e$ and $4e$ channels. The fit components modeling $f$ and $\gamma$ contributions are also shown.
   \label{fig:ThreePlusOneFit}}
 \end{figure}


%%%%%%%%% sPLOT $$$$$$$$$$$
 The \emph{sPlot} method~\cite{Pivk2005356} is used to obtain the contributions
of the different background sources during the fit to \nInnerPix to the distribution of the data in
$\pt$ (or any other kinematic variable).
The \emph{sPlot} tool, as implemented in ROOT, returns for each event a covariance-weighted quantity (\emph{sWeight}) for each component of the fit, which
corresponds in this case to the probability that the $X$ object in the event is
$f$ or $\gamma$.
The distribution for each background component in the CR is obtained by
summing up the \emph{sWeight} from all events.

\paragraph*{Selection efficiencies}
The efficiency of a background object to pass the nominal electron
selections, namely the full electron identification (with the \nInnerPix hit requirement) 
together with the isolation selections, is required in order to extrapolate the fitted background
yields in the $3\ell$+$X$ CR to the signal region.

The efficiency is estimated separately for the different background components ($f$, $\gamma$)
from MC simulation in the $Z$+$X$ CR, after having corrected the
simulation so that it better reproduces the efficiencies measured in data.
%The measurement of these efficiencies in data and the extracted
%data/MC scale factors are detailed in \refS{subsec:ZplusEl}.
The resulting scale factors come with a relative systematic
uncertainty of about $23\%$ for $f$ and $\gamma$,
taking into account residual MC mismodeling, uncertainty on the heavy
flavour contribution and the statistical precision of the efficiencies
from data and MC.
This uncertainty on the scale factors is the dominant one on the final
estimate.
 
\paragraph*{Estimation in the SR}

The final estimation for $f$ and $\gamma$ background in the SR is
obtained separately for the two components with the simple function
$$N_{\textrm{SR}} = \sum_{i} s_i \sum_{j} \varepsilon_ij \times N^{ij}_{sPlot}~~,$$
where the index of the sum runs over $\pt$ intervals i and $n_{jet}$ intervals j, $\varepsilon$ is the
efficiency for the given background component, $s$ is the corresponding
$\pt$ efficiency scale factor and
$N_{sPlot}$ is the corresponding \emph{sWeight} sum.
All variables in this formula are defined in $\pt$-$n_{jet}$ intervals excepting the efficiency scale factor.

\begin{table}[h]
  \begin{center}
    \caption{Fit result for yields in
      the $3\ell$+$X$ CR with statistical errors, shown together
      with the ZZ+HF contamination and the efficiency used to extrapolate the yields
      to the SR. The SR yields for the $f$ and $\gamma$ components are quoted with statistical uncertainty as
      returned from the data fit and systematic uncertainty of the
      efficiency and the fit. 
      For the $q$ component that is not fitted in the data, the SR yield is taken directly from MC
      simulation and is quoted with its total uncertainty.
      \label{tab:ThreePlusOne_data_results}}
    \vspace{0.2cm}
    \begin{tabular}{ccccc}
      \hline\hline 
      $4e$+$2\mu 2e$ &&&\\
      \hline
		type & data fit  & ZZ+HF & efficiency [\%] & SR yield\\
       \hline
		$f$  & $3048\pm 56$     & $208\pm 4$    & $0.20 \pm 0.01$ & $5.74 \pm 0.11 \pm 0.51$ \\
		$\gamma$ & $210 \pm 17$ & $15.3\pm 0.4$ & $0.67 \pm 0.03$ & $1.3 \pm 0.11 \pm 0.04	$  \\
       		\hline
$q$ & \multicolumn{3}{c}{(MC-based estimation)} & $5.6 \pm 1.7$ \\
       		
       
%		$f$  & $1121\pm 34$    & $0.24 \pm 0.03$ & $2.47 \pm 0.08 \pm 0.34$ \\
%        $\gamma$ & $78 \pm 10$ & $0.76 \pm 0.05$ & $0.56 \pm 0.08 \pm 0.04	$  \\
%        \hline
%        $q$ & \multicolumn{2}{c}{(MC-based estimation)} & $2.45 \pm 0.75$ \\
       
%		$f$  & $793\pm 29$  & $0.25\pm 0.06$ & $1.90 \pm 0.07 \pm 0.29$ \\
%        $\gamma$ & $58 \pm 9$ & $0.77 \pm 0.21$ & $0.44 \pm 0.07 \pm 0.03	$  \\
%        \hline
%        $q$ & \multicolumn{2}{c}{(MC-based estimation)} & $1.6 \pm 0.5$ \\
        %%%%%%%%%%%% CORRECT ONES:
%		$f$  & $793\pm 29$  & $0.25\pm 0.04$ & $1.83 \pm 0.07 \pm 0.28$ \\
%		$\gamma$ & $58 \pm 9$ & $0.77 \pm 0.06$ & $0.42 \pm 0.07 \pm 0.03$  \\
%		\hline
%		$q$ & \multicolumn{2}{c}{(MC-based estimation)} & $1.85 \pm 0.57$ \\

        \hline
      \hline
    \end{tabular} 
  \end{center}
\end{table}

%\begin{table}[!htbp]
%\centering
%\caption{Sherpa and MadGraph estimates of $f$ and $\gamma$ background contributions, statistical uncertainties only. 
%    %\textcolor{red}{Numbers to be updated with new production.}
%    \label{tab:Sherpa_MG_est}}
%\vspace{0.2cm}
%    \begin{tabular}{ccc}
%      \hline\hline
%      & Sherpa & MadGraph \\
%      \hline
%      $f$ & $6.26 \pm 0.12$ & $5.04 \pm 0.10$ \\
%      $\gamma$ & $1.32 \pm 0.11$ & $2.25 \pm 0.20 $ \\
%      $total$ & $7.58 \pm 0.23$ & $7.29 \pm 0.30 $ \\
%      \hline\hline
%    \end{tabular}
%\end{table}

As the heavy flavour contribution is subtracted from the $3\ell$+$X$ CR, it is not considered at all in the extrapolation.
Its contribution in the signal region, mainly originating from $Z$+jets and $\ttbar$, is taken from the MC simulation.
The $Z+b\bar b$ cross-section has been measured by ATLAS~\cite{ATLAS_Zbb} to be in agreement with  the expectation 
from the $Z$+jets generator used to derive our prediction. Moreover, the $Z$+HF production is also checked in 
the $\ell\ell\mu\mu$ inverted $d_0$ CR, where the data and the MC expectation are found to be in perfect 
agreement for this component. The $Z$+HF estimation is assigned a overall systematic of 30\% to account also 
for possible discrepancies in the simulation for the selection efficiencies. 
%For $\ttbar$, the MC sample suffers from large statistical uncertainty of about 45\% which is added to the systematics of the overall estimate.

In~\refT{tab:ThreePlusOne_data_results} the estimation of each background component in the SR is
shown together with the yield from the data fit (and the ZZ+HF contamination that must be subtracted from it) 
and efficiency averaged over $\pt$.
The table also quotes the systematic uncertainties on the estimates of the different background
components.
The dominant uncertainties are systematic and have to do with the precision on the selection efficiencies obtained from the $Z$+$X$ sample for $f$ and $\gamma$ estimates and with the MC-based estimation for the heavy-flavour component.

%As the efficiencies used to get the estimation are mc-based, in principle they depend on the physics modelling of the generator and they can vary significantly (see Section \ref{subsec:ZplusEl}). Nevertheless, the mc-based efficiencies are scaled to represent the data, thus such differences are absorbed in the data/mc scale factors. In addition to the baseline MC sample for Z+jets (Sherpa), MadGraph is also used to study the effect on the analysis. The comparison is shown in Table~\ref{tab:Sherpa_MG_est} (for $\pT$ efficiencies only, since scale factors are calculated only with respect to $\pt$) and no difference is seen on the final estimate.
