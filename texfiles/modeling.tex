
The discriminat variable employed is the invariant mass of the four lepton, \mfl.
A unbiased modeling of \mfl is enssential for the search. 
In \RunOne publication~\cite{Aad:2015kna}, 
smoothed templates are used to model the signal and background,
which demands large amount of fully simulated events.
However, in this thesis analytical parameterizations of signal and ZZ continuum background are achieved
and valided against the fully simulated events with reasonable amount of statistics.
The fully parameterized signal modeling enables to explore the large width assumptions (LWA) 
and interference effects in a coherent manner.
Section~\ref{sec:signal_modeling} described the signal modeling for narraw width approximation (NWA) and LWA,
followed by Section~\ref{sec:signal_acc} presenting the acceptance of different signal hypothese. 
Section~\ref{sec:bkg_model} encodes the empirical parameterization for the ZZ continuum background.

\subsection{Signal modeling for Narrow Width Approximation}
\label{sec:signal_modeling}
In the case of a narrow width resonance, the width of the reconstructed four lepton invariant mass \mfl
is determined essentially by the detector resolution,
which can be modeled by the sum of a Crystal Ball ($\mathcal{CB}$) function and a Gaussian.  
\begin{equation}
    \label{eq:sig_model}
    P_{s} (\mfl) = f_{\text{CB}} \times \mathcal{CB}(\mfl; \mu, \sigma_{\text{CB}}, \alpha_{\text{CB}}, n_{\text{CB}}) + (1 - f_{\text{CB}}) \times Gauss(\mfl; \mu, \sigma_{\text{gauss}})
\end{equation}
where the Crystal Ball function is: 
\begin{equation}
    \label{eq:cb_function}
    \begin{aligned}
        \mathcal{CB}(x; \mu, \sigma, \alpha, n) &= N \cdot \left\{
        \begin{aligned}
            \exp(-\frac{x - \mu}{2\sigma^2}),\;& \text{ for } \frac{x - \mu}{\sigma} > -\alpha \\
            A \cdot (B - \frac{x - \mu}{\sigma})^{-n},\;& \text{ for } \frac{x - \mu}{\sigma} \leq -\alpha
        \end{aligned}
        \right. \\
        &\text{N is a normalization factor.} \\
        A &= (\frac{n}{|\alpha|})^n \cdot \exp(-\frac{|\alpha|^2}{2}), \\
    B &= \frac{n}{|\alpha|} - |\alpha|. 
    \end{aligned}
\end{equation}

The $\mathcal{CB}$ and the Gaussian function share the same mean value of \mfl ($\mu$),
but acquire different parameters for the resolution, $\sigma_{\text{CB}}$ and $\sigma_{\text{Gauss}}$,
to cover the bulk and tail part separately.
The $\alpha_{\text{CB}}$ and $n_{\text{CB}}$ controls the power tails of the $\mathcal{CB}$,
 and the $f_{\text{CB}}$ defines the fraction of the contribution from $\mathcal{CB}$\@.
The ggF and VBF production yield the same \mfl spectrum, thus only the modeling for ggF production is shown below.
Channels of $2e2\mu$ and $2\mu2e$ are combined without loosing sensitivities for $\mfl > 180 \gev$ since 
the two Z boson are mostly on-shell.
Alternative modeling using double Gaussian has been checked, has worse modeling of the tail part of \mfl.

Using the above modeling of Equation~\ref{eq:sig_model} a fit is performed on the simulated signal events that is generated with a hypothesised \mH,
to obtain the best estimates of the parameters for the mass. 
The \mfl distribution, along with the fit projection, of the signal hypotheses with $\mH \in$ [200, 1000] \gev\ are presented in Figures~\ref{fig:gg_mass_signalParam_4mu}, \ref{fig:gg_mass_signalParam_4e} and \ref{fig:gg_mass_signalParam_2mu2e},
for $4\mu$, $4e$, $2e2\mu$, respectively. %Overall, a good fit quality is obtained in the whole mass range. 
The best estimates of the parameters are then parametrized as a function of \mH as shown in Figures~\ref{fig:gg_FitParametersInterpolation_4mu}, \ref{fig:gg_FitParametersInterpolation_4e}, and \ref{fig:gg_FitParametersInterpolation_2mu2e}  for each channel,
using a linear function for the mean $\mu$, second order polynomial functions for the parameters $f_{CB}$, $\alpha_{CB}$, $\sigma_{CB}$ and $\sigma_{Gauss}$. 
The best estimates of the parameter $n_{CB}$ in the Crystal Ball has small variations, therefore are set to the averaged value of the estimates as constant: $n_{CB}=3.5$ for $4\mu$ and 1.5 for the combined $2\mu 2e$ and $2e 2\mu$ channels, and $n_{CB}=1.9$ for $4e$ channel. 
The quality of the fit is estimated by computing the Pearson's $\chi^{2}$, the values of which are summarised in Table \ref{tab:chi2}.

%
Possible bias on the extracted signal yield due to the signal parametrization with respect to the yield in the simulation is studied. 
A closure test is performed by comparing the number of reconstructed events extracted from the fit with that directly from the simulation of input.
Figure ~\ref{fig:gg_graph_YieldCheckAll} shows $\frac{N_{\text{Sim}}-N_{\text{Fitted}}}{N_{\text{Fitted}}}$, where $N_{\text{Sim}}$ is the number of reconstructed events from input simulation and $N_{\text{Fitted}}$ is the one from the fit. 
The errors shown in the fit are statistical only. 
The bias extracted on the signal yield is below 1\% in all channels and is hence considered negligible. 

Figure~\ref{fig:gg_signalTemplates} summarizes the \mfl distribution of the parametrized signal hypotheses for $4\mu$, $4e$ and $2e2\mu+2\mu 2e$ final states. 

%---------------------------------------
\begin{table}[!htpb]
  \centering
  \caption{\label{tab:chi2}Summary of the $\chi^{2}$ values obtained when estimating the parameters of Equation 1 with analytical fits to ggF MC distributions in ggF category.}
  \begin{tabular}{ l  l  l  l }
    \hline \hline
	$\chi ^{2}$ & 4$\mu$ & $4e$ & 2$\mu$2e \\
	\hline
	200  & 1.2 & 1.2 &  1.3 \\
	300  & 1.8 & 1.0   &   1.1 \\
	400  & 1.5 &  1.1 &  1.5 \\
	500  & 0.9 & 1.4 & 0.9 \\
	600  & 1.6 &  1.0 & 1.2 \\
	700  & 1.4 &  1.1 & 1.6 \\
	800  & 1.2 &  1.3 & 1.0 \\
	900  & 1.5 & 1.0 &  1.3 \\
	1000 & 1.2 & 0.7 &  1.2 \\
    \hline \hline
  \end{tabular}
\end{table}


%---------------------------------------
\begin{figure}[!htbp]
\begin{center}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_200_H4l_4mu.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_300_H4l_4mu.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_400_H4l_4mu.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_500_H4l_4mu.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_600_H4l_4mu.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_700_H4l_4mu.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_800_H4l_4mu.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_900_H4l_4mu.eps}\\
%\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_1000_H4l_4mu.eps}
\end{center}
\caption{Distributions of $m_{4\mu}$ invariant mass fit projection of the signal samples from mass of 200 to 1000 GeV in steps of 100 GeV. Low panel in each plot shows the pull distribution.}
\label{fig:gg_mass_signalParam_4mu}
\end{figure}
%---------------------------------------
\begin{figure}[!htbp]
\begin{center}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_200_H4l_4e.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_300_H4l_4e.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_400_H4l_4e.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_500_H4l_4e.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_600_H4l_4e.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_700_H4l_4e.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_800_H4l_4e.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_900_H4l_4e.eps}\\
%\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_1000_H4l_4e.eps}
\end{center}
\caption{Distributions of $m_{4e}$ invariant mass fit projection of the signal samples from mass of 200 to 1000 GeV in steps of 100 GeV for NWA. Low panel in each plot shows the pull distribution. }
\label{fig:gg_mass_signalParam_4e}
\end{figure}
%---------------------------------------
\begin{figure}[!htbp]
\begin{center}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_200_H4l_2mu2e.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_300_H4l_2mu2e.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_400_H4l_2mu2e.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_500_H4l_2mu2e.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_600_H4l_2mu2e.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_700_H4l_2mu2e.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_800_H4l_2mu2e.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_900_H4l_2mu2e.eps}\\
%\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_mass_signal_1000_H4l_2mu2e.eps}
\end{center}
\caption{Distributions of $m_{2\mu 2e}$ invariant mass fit projection of the signal samples from mass of 200 to 1000 GeV in steps of 100 GeV for NWA.  Low panel in each plot shows the pull distribution.}
\label{fig:gg_mass_signalParam_2mu2e}
\end{figure}
%--------------------------------------- 
\begin{figure}[!htbp]
\begin{center}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_graph_mass_mean_4mu_fit.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_graph_f_cb_gauss_4mu_fit.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_graph_mass_gauss_sigma_4mu_fit.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_graph_mass_cb_sigma_4mu_fit.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_graph_mass_cb_alpha_4mu_fit.eps}
\end{center}
\caption{Interpolation of the parameters $\mu$,  $f_{CB}$, $\sigma_{Gauss}$, $\sigma_{CB}$ and $\alpha_{CB}$ of the signal model in the  $4\mu$  channel as a function of the $m_{H}$. $n_{CB}$ in the Crystall-Ball is set to 10 as constant.}
\label{fig:gg_FitParametersInterpolation_4mu}
\end{figure}

%---------------------------------------
\begin{figure}[!htbp]
\begin{center}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_graph_mass_mean_4e_fit.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_graph_f_cb_gauss_4e_fit.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_graph_mass_gauss_sigma_4e_fit.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_graph_mass_cb_sigma_4e_fit.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_graph_mass_cb_alpha_4e_fit.eps}
\end{center}
\caption{Interpolation of the parameters $\mu$,  $f_{CB}$, $\sigma_{Gauss}$, $\sigma_{CB}$ and $\alpha_{CB}$ of the signal model in the  4e  channel as a function of the $m_{H}$. $n_{CB}$ in the Crystall-Ball is set to 2 as constant.}
\label{fig:gg_FitParametersInterpolation_4e}
\end{figure}
%---------------------------------------
\begin{figure}[!htbp]
\begin{center}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_graph_mass_mean_2mu2e_fit.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_graph_f_cb_gauss_2mu2e_fit.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_graph_mass_gauss_sigma_2mu2e_fit.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_graph_mass_cb_sigma_2mu2e_fit.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/gg_graph_mass_cb_alpha_2mu2e_fit.eps}
\end{center}
\caption{Interpolation of the parameters $\mu$,  $f_{CB}$, $\sigma_{Gauss}$, $\sigma_{CB}$ and $\alpha_{CB}$ of the signal model in the  mixed channels $2\mu 2e$ and $2e 2\mu$ as a function of the ${mH}$. $n_{CB}$ in the Crystall-Ball is set to 10 as constant.}
\label{fig:gg_FitParametersInterpolation_2mu2e}
\end{figure}
%---------------------------------------
\begin{figure}[!htbp]
\begin{center}
\includegraphics[width=.45\textwidth]{figures/signalParametrisation/gg_graph_YieldCheck_4mu.eps}
\includegraphics[width=.45\textwidth]{figures/signalParametrisation/gg_graph_YieldCheck_4e.eps}\\
\includegraphics[width=.45\textwidth]{figures/signalParametrisation/gg_graph_YieldCheck_2mu2e.eps}
\end{center}
    \caption{The bias introduced by the signal extraction is estimated as $\frac{N_{\text{Sim}}-N_{\text{Fitted}}}{N_{\text{Fitted}}}$, where $N_{\text{sim}}$ is the number of reconstructed events and $N_{\text{Fitted}}$ is the number of events obtained from the fit. $4\mu$ (left), $4e$ (right) and mixed channels $2\mu 2e$ and $2e 2\mu$ (bottom).}
\label{fig:gg_graph_YieldCheckAll}
\end{figure}
%---------------------------------------
\begin{figure}[!htbp]
\begin{center}
\includegraphics[width=.45\textwidth]{figures/signalParametrisation/gg_multipeakplot_4mu.eps}
\includegraphics[width=.45\textwidth]{figures/signalParametrisation/gg_multipeakplot_4e.eps}\\
\includegraphics[width=.45\textwidth]{figures/signalParametrisation/gg_multipeakplot_2mu2e.eps}
\end{center}
\caption{The \mfl ditribution of each signal hypothesis for $4\mu$ (left), $4e$ (right) and mixed channels $2\mu 2e$ and $2e 2\mu$ (bottom).}
\label{fig:gg_signalTemplates}
\end{figure}

%% VBF-like category...
In the VBF-like category, the \mfl spectrum includes the events from $4mu$, $4e$, $2mu2e$, which have so different mass resolutions that 
a simple modeling like Equation~\ref{eq:sig_model} will not work properly. 
To cope with that, a sum of three Equation~\ref{eq:sig_model} is used, where each Equation~\ref{eq:sig_model} describes a final state and is normalized according to the expection from the simulation. Eventually a closure test is obtained. \textcolor{red}{add some plots please..}

\clearpage
\subsection{Signal modeling for Large Width Assumption}  \label{sec:LWAParametrisation}

\paragraph{signal only modeling}
The $m_{4\ell}$ shape of the signal under large width assumption can be modeled by convolving the theoretical differential distribution of \mfl with the detector resolution.
The modeling of detector resolution is essentially the modeling for the NWA as described above.
The differential cross section for a heavy scalar can be analytically expressed as that in Ref~\cite{Goria:2011wa}:
\begin{equation}
\sigma_{gg \to H \to ZZ} (s) = \frac{1}{2s}  \int d \Omega \left | A_{gg \to H}(s,\Omega) \right |^2 \frac{1}{ \left | s - s_H \right |^2}  \left | A_{H \to ZZ}(s,\Omega) \right |^2
\end{equation}

where $A_{gg \to H}(s,\Omega)$ and $A_{H \to ZZ}(s,\Omega)$ are corresponding Higgs production and decay amplitudes, $s$ is the virtuality and $s_H$ is the complex pole for the Heavy Higgs,
$\frac{1}{ s - s_H}$ is a Higgs propagator (see Equation~\ref{eq:propagator}) and $\Omega$ is a phase space of the process.

Using the definition of a partial width:

\begin{equation}
\Gamma_{H \to F} (s) = \frac{1}{2\sqrt{s}}  \int d \Omega \left | A_{H \to F}(s,\Omega) \right |^2 
\label{eq:PartialWidth}
\end{equation}

the parton cross section can be rewritten as:

\begin{equation}
    \begin{split}
        \sigma_{gg \to H \to ZZ} (s) &= \frac{1}{\pi}\sigma_{gg\to H} \frac{s^2}{ \left | s - s_H \right |^2} \frac{\Gamma_{H \to ZZ}}{\sqs}  \\
        &=2  \frac{1}{ \left | s - s_H \right |^2}  \times \Gamma_{H \to gg} (s) \times \Gamma_{H \to ZZ} (s)
    \end{split}
\label{eq:HiggsPartonXSection}
\end{equation}

where the components are computed in \cite{Goria:2011wa}, \cite{Spira1995}:

\begin{equation}
\begin{split}
    \frac{1}{s - s_H} &=\frac{ 1 + i \cdot \overline{\Gamma}_H / \overline{m}_H} { s - \overline{m}_H^2 +  i \cdot s \cdot\overline{\Gamma}_H / \overline{m}_H } \\
    \overline{m}_H &= \sqrt{\Gamma_H^2 + m_H^2} \\
    \overline{\Gamma}_H &= \overline{m}_H  \cdot \frac{\Gamma_H}{m_H}
 \end{split}
\label{eq:propagator}
\end{equation}

The Equation~\ref{eq:propagator} is the identity for the propagator following the Bar-scheme.

\begin{equation}
\Gamma_{H \to ZZ} (s) = C \cdot s^{\frac{3}{2}} \cdot \left [ 1 - \frac{4m_Z^2}{s} + \frac{3}{4}\left ( \frac{4m_Z^2}{s} \right )^2 \right ] \cdot \left [ 1 - \frac{4m_Z^2}{s} \right ]^{\frac{1}{2}}
\label{eq:WidthZZ}
\end{equation}

\begin{equation}
\begin{split}
    \Gamma_{H \to gg} (s) &= C \cdot s^{\frac{3}{2}} \cdot \left | A_t(\tau_t) \right |^{2} \\
    A_t(\tau) &= 2 \frac{\tau+(\tau-1)f(\tau)}{\tau^2} \\
    \tau_t &= \frac{s}{4m_t^2} \\
    f(\tau) &= \left\{\begin{matrix}
arcsin^2 (\sqrt{\tau}) , ~ \tau \leqslant 1
\\ 
- \frac{1}{4} \left [  \log{\frac{1+\sqrt{1-\tau^{-1}}}{1-\sqrt{1-\tau^{-1}}}}  - i  \pi \right ]^2 , ~ \tau > 1
\end{matrix}\right.
 \end{split}
\label{eq:WidthGG}
\end{equation}

$m_t$ corresponds to mass of top quark, $m_Z$ is the mass of Z boson, and $\Gamma_H$ is an assumed total width of the Heavy Higgs boson.
In the calculation of $\Gamma_{H \to gg}$, it is assumed the contribution from top quark dominates and that from other quarks is ignored.

In the case of LHC, the differential distribution of the $m_{4\ell}$ is defined by a hadron cross section that can be derived from Equation \ref{eq:HiggsPartonXSection} by multiplying by the gluon-gluon luminosity $\mathcal{L}_{gg}$ described in Ref~\cite{Ball:2012wy}: 
\begin{equation}
    \mathcal{L}_{gg}(\nu) = \int_\nu^1 \frac{dx}{x}\,f_g(x, \mu_F)f_g(\frac{\nu}{x}, \mu_F)
\end{equation}
where $f_i$ is a parton distribution function, $\mu_F$ is the factorization scale.

Also cross section should be rewritten as a function of $m_{4\ell}$ but not $s$, that will give and extra power of mass dependence in the formula.

\begin{equation}
%\sigma_{pp \to H \to ZZ} (m_{4\ell}) 
    \frac{d\sigma_{pp \to H \to ZZ}}{d m_{4\ell}}=2 \cdot m_{4\ell} \cdot \mathcal{L}_{gg} \cdot  \frac{1}{ \left | s - s_H \right |^2}  \cdot \Gamma_{H \to gg} (m_{4\ell}^2) \cdot \Gamma_{H \to ZZ} (m_{4\ell}^2)
\label{eq:HiggsHadronXSection}
\end{equation}

Comparison of the analytical shape to a truth $m_{4\ell}$ distribution in gg2VV MC samples is shown on Figure \ref{fig:truthShape}. Good agreement is found only for masses above 300 \gev.

\begin{figure}[!htbp]
\begin{center}
\includegraphics[width=.3\textwidth]{figures/signalParametrisation/LWA/450_5.png}
\includegraphics[width=.3\textwidth]{figures/signalParametrisation/LWA/450_10.png}
\includegraphics[width=.3\textwidth]{figures/signalParametrisation/LWA/450_15.png}\\
\includegraphics[width=.3\textwidth]{figures/signalParametrisation/LWA/700_5.png}
\includegraphics[width=.3\textwidth]{figures/signalParametrisation/LWA/700_10.png}
\includegraphics[width=.3\textwidth]{figures/signalParametrisation/LWA/700_15.png}\\
\end{center}
\caption{Comparison of the analytical shape to a truth $m_{4\ell}$ distribution in gg2VV MC samples for 450 GeV, 700 GeV and width equal to 5,10,15 $\%$ of the mass}
\label{fig:truthShape}
\end{figure}

Reconstructed distribution can be modelled as the analytical modeling~\ref{eq:HiggsHadronXSection} multiplied by acceptance and convolved with the detector effects. 
Comparison of the modelled shape to the MC distribution is shown on Figures ~\ref{fig:recoShape_4mu}, ~\ref{fig:recoShape_4e} and ~\ref{fig:recoShape_2mu2e}.

%---------------------------------------
\begin{figure}[!htbp]
\begin{center}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_400_fixed_H4l_4mu.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_500_fixed_H4l_4mu.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_600_fixed_H4l_4mu.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_700_fixed_H4l_4mu.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_800_fixed_H4l_4mu.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_900_fixed_H4l_4mu.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_1000_fixed_H4l_4mu.eps}
\end{center}
\caption{Comparison of the analytical shape convoluted with detector effects to reconstructed $m_{4\mu}$ MC distribution for 400-1000 GeV masses and  width equal to $15\%$ of the mass.}
\label{fig:recoShape_4mu}
\end{figure}
%---------------------------------------
\begin{figure}[!htbp]
\begin{center}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_400_fixed_H4l_4e.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_500_fixed_H4l_4e.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_600_fixed_H4l_4e.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_700_fixed_H4l_4e.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_800_fixed_H4l_4e.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_900_fixed_H4l_4e.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_1000_fixed_H4l_4e.eps}
\end{center}
\caption{Comparison of the analytical shape convoluted with detector effects to reconstructed $m_{4e}$ MC distribution for 400-1000 GeV masses and  width equal to $15\%$ of the mass.}
\label{fig:recoShape_4e}
\end{figure}
%---------------------------------------
\begin{figure}[!htbp]
\begin{center}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_400_fixed_H4l_2mu2e.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_500_fixed_H4l_2mu2e.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_600_fixed_H4l_2mu2e.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_700_fixed_H4l_2mu2e.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_800_fixed_H4l_2mu2e.eps}
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_900_fixed_H4l_2mu2e.eps}\\
\includegraphics[width=.4\textwidth]{figures/signalParametrisation/LWA/gg_mass_signal_1000_fixed_H4l_2mu2e.eps}
\end{center}
\caption{Comparison of the analytical shape convoluted with detector effects to reconstructed $m_{2\mu2e}$ MC distribution for 400-1000 GeV masses and  width equal to $15\%$ of the mass.}
\label{fig:recoShape_2mu2e}
\end{figure}

\include{texfiles/interference}
%---------------------------------------
\clearpage
\subsection{Signal acceptance} \label{sec:signal_acc}
The signal acceptance is estimated using the same MC samples as used for the signal shapes and 
calculated for each production mode per category.
It is the ratio of the number of events passing the reconstruction-level cuts in each category ($4\mu$, $2\mu2e$, $4e$ and VBF-like)
over the total generated  $H\ra ZZ\ra 4\ell$ events (where $\ell=\mu, e, \tau$). 
The events from Z to di-tau decays only contributes 0.5\% of the events passing the reco-level cuts, thus are neglected.
Since this analysis only targets the channels of Z decays to di-electron or di-muon, the acceptance derived directly from MC simulation are corrected by a factor of 9/4.
By definition the signal acceptance includes both the geometrical acceptance and the selection efficiencies. 
The acceptance is parametrised by a 2nd order polynomial in \mH, separately for each production mode and category.  
The acceptance functions for ggF and VBF production in the NWA are shown in \refF{fig:acceptances}, with the parameters
recorded in Table~\ref{tab:acceptanceparams}.  
The degree of the polynomial was chosen to ensure a
smooth shape across the full \mH range, which both describes the simulation well (requiring at
least 2nd order), and does not overfit the statistical variations in the simulation (a problem
appearing at 3rd order and above). A systematic uncertainty of approximately $1\%$ was assigned based
on the deviation of the individual mass points from the best fit line in each channel.

Using the ggH MC simulation with a width $\Gamma_S = 0.15 \times m_S$, the acceptance for LWA was compared against NWA. These comparisons are found in \refF{fig:acceptancesLWA}. 
The difference observed of the ggF acceptance into the VBF category arises from the different simulations used for parton showering: the NWA is simulated with Powheg, while the LWA with \MCatNLO. 
The difference is accounted for by the systematic uncertainty discussed in \refS{sec:theorysys}.
For the ggF acceptance into the $4e$, $4\mu$, and $2\mu2e$ categories, the difference is purely from MC statistics. For this reason the LWA analysis uses the same acceptance functions as the NWA analysis, for both ggF and VBF production.


\begin{table}[!htpb]
  \centering
    \caption{The polynomial parameters for signal acceptance for each production mode and each category.
    \label{tab:acceptanceparams}
    }

  \begin{tabular}{| c || c | c | c | c | c |}
    \hline
             & $a_{0}$  & $a_{1}[/GeV]$  & $a_{2}[/GeV^2]$  \\
    \hline \hline
    ggF production    \\
    \hline
    $4\mu$ & 1.171e-01 & 8.621e-05 & -3.916e-08 \\
    $4e$   & 7.246e-02 & 1.108e-04 & -4.184e-08 \\
    $2\mu2e$ & 1.875e-01 & 2.035e-04 & -8.393e-08 \\
    $VBF$ & 7.132e-04 & 6.337e-05 & -1.770e-08 \\
    \hline \hline
    VBF production   \\
    \hline
    $4\mu$ & 8.263e-02 & 1.970e-05 & -2.195e-09 \\
    $4e$     & 4.988e-02 & 5.538e-05 & -1.732e-08 \\
    $2\mu2e$  & 1.312e-01 & 8.359e-05 & -2.609e-08 \\
    $VBF$ & 1.456e-01 & 2.614e-04 & -1.173e-07 \\
    \hline \hline
  \end{tabular}
\end{table}


\begin{figure}[!htbp]
\begin{center}
\includegraphics[width=.45\textwidth]{figures/Acceptance/acceptance_ggH_NW_pol2.eps}
\includegraphics[width=.45\textwidth]{figures/Acceptance/acceptance_VBFH_NW_pol2.eps}
\end{center}
\caption{The signal acceptance vs $m_{H}$ for the ggF (left) and VBF (right) production. For each production mode, acceptance into the four categories ($4\mu$, $4e$, $2\mu2e$, VBF) are parameterized and fit separately. The fit parameters are shown in Table~\ref{tab:acceptanceparams}.}
\label{fig:acceptances}
\end{figure}

\begin{figure}[!htbp]
\begin{center}
\includegraphics[width=.45\textwidth]{figures/Acceptance/AC_ratio_ggF_4e.eps} 
\includegraphics[width=.45\textwidth]{figures/Acceptance/AC_ratio_ggF_4mu.eps} 
\includegraphics[width=.45\textwidth]{figures/Acceptance/AC_ratio_ggF_2mu2e.eps} 
\includegraphics[width=.45\textwidth]{figures/Acceptance/AC_ratio_VBF.eps} 
\end{center}
\caption{Ratio of the acceptance in simulated samples with different mass and width over the NWA parameterization for signal acceptance.}
\label{fig:acceptancesLWA}
\end{figure}

\clearpage
\subsection{Background modelling}
\label{sec:bkg_model}
The \mfl distribution for the ZZ continuum background is taken from MC simulation, and parameterized by an empirical function: 

\begin{equation}
    \label{eq:ggZZ_model}
    f_{qqZZ/ggZZ}(m_{4\ell}) = (f_{1} (m_{4\ell})+ f_{2} (m_{4\ell})) \times H(m_{4\ell}-m_{0}) \times C_0 + f_{3} (m_{4\ell}) \times H(m_{0}-m_{4\ell})
\end{equation}

where:

\begin{equation}
\begin{split}
    f_{1} (m_{4\ell}) &= \exp(a_1 + a_2 \cdot m_{4\ell})  \\
    f_{2} (m_{4\ell}) &= (\frac{1}{2} + \frac{1}{2} erf(\frac{m_{4\ell}-b_1}{b_2})) \times \frac{1}{1+\exp(\frac{m_{4\ell}-b_1}{b_3})} \\
    f_{3} (m_{4\ell}) &=  \exp(c_1 + c_2 \cdot m_{4\ell} + c_3 \cdot m_{4\ell}^2  + c_4 \cdot m_{4\ell}^{2.7}) \\
    C_0 &= \frac{f_{3} (m_0)} {f_{1} (m_0)+f_{2} (m_0)}
\end{split}
\label{eq::bkg_model2}
\end{equation}

The $f_1$ covers the flat low mass part of the spectrum where one of Z bosons is off-shell, while $f_2$ models the ZZ threshold around 2$\cdot m_Z$ and $f_3$ describes the high mass tail. 
The transition between low mass and high mass parts is done by the Heaviside step function $H(x)$ around $m_0 = 240$ \gev. 
The continuity of the function around the $m_0$ is ensured by the normalisation factor $C_0$ that is applied to the low mass part. Finally, $a_i$, $b_i$ and $c_i$ are shape parameters which are obtained by fitting the simulated \mfl for each category.

