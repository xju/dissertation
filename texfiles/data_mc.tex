\clearpage
\chapter{Data and Monte Carlo samples} 
\label{ch:data_mc}

\section{Data samples}
The observation of a SM Higgs boson uses the $pp$ collision data
collected by the ATLAS detector during 2011 and 2012 at 
center-of-mass energies of \sqs = 7 and 8~\tev\ with a bunch spacing of 50~ns.
Only events taken in stable beam conditions, 
and in which the trigger system, the tracking devices, 
the calorimeters and the muon chambers
were functioning as expected, are considered in the physics analysis. 
The resulting effective luminosity for 7~\tev\ $pp$ data is 4.5~\ifb,
and that for 8~\tev\ $pp$ data is 20.3~\ifb.
The overall uncertainty on the integrated luminosity for the complete 
2011 data set is $\pm1.8\%$~\cite{Aad:2013ucp}.
The uncertainty on the integrated luminosity for the 2012 data set
is $\pm2.8\%$; this uncertainty is derived following the methodology
used the 2011 data set, from a preliminary calibration of the 
luminosity scale with beam-separation scans performed in 
November 2012.

The search for additional heavy scalars uses the $pp$ collision data 
collected by the ATLAS detector in 2015 and 2016 at a center-of-mass energy of 
\sqs = 13~\tev\ with a bunch spacing of 25~ns.
The effective integrated luminosity used in the physics analysis
 for 2015 and 2016 data set is \lumiTot.
The uncertainty on the integrated luminosity of 2015 and 2016 data set is $\pm3.2\%$.
This is derived , following a methodology similar to that detailed in Ref.~\cite{lumi2016}, 
from a preliminary calibration of the luminosity scale using $x$-$y$ beam 
separation scans performed in May 2016.

\section{Monte Carlo samples}
\label{sec:mc}
%The fully simulated Monte Carlo samples (MC samples) in ATLAS framework are produced in four 
\subsection{Overview}
The ATLAS simulation software chain~\cite{simulation2010} is 
generally divided into three steps:
(1) the generation of the event for immediate decays, 
which can be carried out by different generators, following 
the Les Houches Event file format~\cite{Alwall2007300};
(2) the simulation of the detector and soft interactions within the GEANT4 
framework~\cite{GEANT4, Allison:2006ve};
and (3) the digitization of the energy deposited in sensitive regions of the detector into
voltages and currents for comparison to the readout of the ATLAS detector. 
The output of the simulation chain can be presented in a format 
identical to the output of the ATLAS data acquisition system (DAQ). 
Thus both the simulated and observed data can 
then be run through the same ATLAS trigger and physics reconstruction packages.

In each step, intermediate files are produced and named differently. 
File from event generation are usually named as \textit{EVNT},
in which information called ``truth'' is recorded for each event.
The truth information is a history of the interactions from the generator, 
including incoming and outgoing particles.
A record is kept for every particle, whether the particle is to be passed through the detector simulation or not.
Files from the simulation of the ATLAS detector are named as \textit{HITS},
which are then treated as the inputs of the digitization 
together with additional simulated HITS files for minimum bias, beam halo, beam gas and 
cavern background events .
These additional simulated HITS files are used to mimic the underlying soft events, particularly
the minimum bias files that simulate the ``pileup events'', which become more important
as the instantaneous luminosity increases.
The Monte Carlo (MC) generator used for pileup events is 
\PYTHIAV{8.212}~\cite{Sjostrand2015159} with either the A14~\cite{ATL-PHYS-PUB-2014-021} set of
tuned parameters and NNPDF23~\cite{Ball:2012cx} for the parton density functions (PDF) set,
or the AZNLO~\cite{STDM-2012-23} tuned parameters
and \progname{CTEQL1}~\cite{Pumplin:2002vw} PDF when the \POWHEGBOX~\cite{Nason:2004rx, Frixione:2007vw} generator is used for the hard process.
The simulated events are weighted to reproduce the
observed distribution of the mean number of interactions per bunch crossing in the data (pileup reweighting).
The properties of the bottom and charm hadron decays are simulated by the \progname{EvtGen} v1.2.0
program~\cite{Lange:2001uf}.
Files from digitization for simulated events or from DAQ for data are named as ``Raw Data Object'' (RDO) file.
Finally, files from reconstruction are named as ``Analysis Object Data'' (AOD) file, which in principle 
are already suitable for each analysis.
However in practice analysis teams use derived AOD files that have imposed coarse event selections and
pruned away redundant information so that turn-around time for the analysis gets shorter.

\subsection{Signal Monte Carlo samples}
The SM Higgs boson signal is modeled using the 
\POWHEGBOX generator~\cite{Nason:2004rx, Frixione:2007vw, powheg4, powheg5},
which calculates separately 
the gluon-gluon fusion and weak-boson fusion production
with matrix elements up to next-to-leading order (NLO) in QCD coupling constant.
The description of the Higgs boson transverse momentum (\pt) spectrum in the
ggF process is re-weighted to follow the calculation of 
Ref.~\cite{deFlorian:2012mx, Grazzini:2013mca}, 
which include QCD corrections up to next-to-next-to-leading order (NNLO)
and QCD soft-gluon resummations up to next-to-next-to-leading logarithm (NNLL).
The effects of non-zero quark masses are also taken into account~\cite{Bagnaschi:2011tu}.
\POWHEGBOX is interfaced to \PYTHIA8~\cite{Sjostrand:2007gs,Sjostrand2015159} for
showering and hadronization, which in turn is interfaced to 
\textsc{Photos}~\cite{Golonka:2005pn, Davidson:2010ew}
for QED radiative corrections in the final state.
\PYTHIA8 is used to simulate the production of a Higgs boson in associated with a W
or a Z boson (VH) or with a  $t\bar{t}$ pair (\ttH). 
The production of a Higgs boson in association with a $b\bar{b}$ pair ($b\bar{b}H$)
is included in the signal yield assuming the same \mH dependence as for the \ttH
process, while the signal efficiency is assumed to be equal to that for ggF production.

In the search for additional scalars, 
 heavy scalar events are produced using the \POWHEG~\cite{powheg4, powheg5} 
generator, which calculates separately 
the gluon-gluon fusion and weak-boson fusion production
with matrix elements up to next-to-leading order (NLO) in QCD coupling constant.
The generated events then are interfaced to \PYTHIA8~\cite{Sjostrand:2007gs,Sjostrand2015159}
for decaying the Higgs boson into the \hzztollll final states as well as for showering and hadronization.
Events from ggF and VBF production are generated separately in the $200 < \mH < 1400$ \gev\ mass
range under the NWA.
In addition, events from ggF production with a width of 15\% of the scalar mass \mH have been generated
with \MGMCatNLO \cite{Alwall:2011uj,md52014} to validate the signal modeling for LWA.
To have better description of the jet multiplicity, \MGMCatNLO is also used to generate the events 
of $pp \to H + \geq 2$ jets at NLO QCD accuracy with the FxFx merging scheme \cite{Frederix2012},
in the Effective Filed Theory (EFT) approach ($m_t \to \infty$). 
The fraction of the ggF events that enter into the VBF-like category is 
estimated from \MGMCatNLO simulation.

\subsection{Background Monte Carlo samples}
The \zz continuum background from $q\bar{q}$ annihilation is simulated with 
the \SHERPA 2.2~\cite{Gleisberg:2008ta, Gleisberg:2008fv,Cascioli:2011va},
with the NNPDF3.0~\cite{Ball:2014uwa} NNLO PDF set for the hard scattering process.
NLO accuracy is achieved in the matrix element calculation for 0-, and 1-jet final states
and LO accuracy for 2- and 3-jet final states.
The merging of jets from hard process and parton shower is performed with 
the \SHERPA parton shower~\cite{Schumann:2007mg}
using the \textsc{MePs}@NLO prescription~\cite{Hoeche:2012yf}.
NLO EW corrections are applied as a function of 
$m_\text{ZZ}$~\cite{Biedermann:2016yvs,Biedermann:2016lvg}.
The EW production of the vector boson scattering with two jets
down to $O(\alpha_{W}^6)$ is generated using \SHERPA, 
where the process $ZZZ\to4\ell qq$ is also taken into account.

The \ggZZ production is modeled by \textsc{Sherpa} 2.2 at LO in QCD,  
including  the off-shell $h$ contribution and the interference between $h$ and the ZZ background. 
The k-factor accounting for higher order QCD effects for the \ggZZ continuum production
has been calculated for massless quark loops \cite{Caola:2015psa,Campbell:2016ivq,Alioli:2016xab} in the heavy top-quark approximation ~\cite{Melnikov:2015laa},
 including the $gg\rightarrow H^{*} \rightarrow ZZ$ processes ~\cite{Li:2015jva}. 
 Based on these studies, a k-factor of 1.7 is used, and a conservative relative uncertainty of 60\% on the normalization is applied to both searches.

%%WZ
The $WZ$ background is modeled using \progname{POWHEG-BOX} v2 interfaced to\progname{PYTHIA 8}
and\progname{EvtGen} v1.2.0. The triboson backgrounds $ZZZ$, $WZZ$, and $WWZ$ with four or more prompt leptons are modeled using\progname{Sherpa} 2.1.1. For the fully leptonic $t\bar{t}+Z$ background, with four prompt leptons coming from the top and $Z$ decays, \progname{MadGraph5\_aMC@NLO} is used.

% Zjets
Events containing $Z$ bosons with associated jets are simulated using the\progname{Sherpa}
2.2.0 generator. Matrix elements are calculated for up to 2 partons at NLO and 4 partons at LO using
the\progname{Comix}~\cite{Gleisberg:2008fv} and\progname{OpenLoops}~\cite{Cascioli:2011va} matrix
element generators and merged with the\progname{Sherpa} parton shower~\cite{Schumann:2007mg} using
the\progname{ME+PS@NLO} prescription~\cite{Hoeche:2012yf}.  The CT10 PDF set is used in conjunction
with dedicated parton shower tuning developed by the\progname{Sherpa} authors. The $Z$ + jets
events are normalized to the NNLO cross sections.

% ttbar
The $t\bar{t}$ background is modeled using\progname{POWHEG-BOX} v2 interfaced to\progname{PYTHIA
  6}~\cite{pythia} for parton shower, fragmentation, and the underlying event and to\progname{EvtGen} v1.2.0 for properties of the bottom and charm hadron decays.