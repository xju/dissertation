% Introduction and Motivation
\clearpage
\chapter{Introduction}
\label{sec:intro} 

The Higgs mechanism~\cite{Englert:1964et,Higgs:1964ia,Higgs:1964pj,Guralnik:1964eu,Higgs:1966ev}
plays a central role in 
%the unification of the electromagnetic 
%and weak interactions by 
providing mass to the W and Z intermediate
vector bosons without violating local gauge invariance.
In the Standard Model~\cite{Glashow:1961tr, Weinberg:1967tq, Salam:1968rm},
the Higgs mechanism implies a neutral and colorless
scalar particle, the Higgs boson.
%or Higgs boson for short 
%(denoted as $h$ in this article).
The search for the Higgs boson is one of the highlights of the Large Hadron
Collider (LHC) physics program~\cite{Evans:2008zzbLHC}.

Indirect experimental limits on the Higgs boson mass of 
$m_{H} < 158 \gev$ at 95\% confidence level (CL) 
are obtained from precision measurements of the
electroweak parameters that depend logarithmically on the Higgs
boson mass through radiative corrections~\cite{ALEPH:2010aa}.
Direct searches at Large Electron-Positron Collider (LEP) at CERN
placed a lower bound of 114.4~\gev\ on the Higgs boson mass
at 95\% CL, using a total of about 2.46~\ipb of $e^{+}e^{-}$
collision data at center-of-mass ($\sqrt{s}$) energies between
189 and 209~\gev~\cite{Barate:2003sz}.
%At the Tevatron proton-antiproton ($p\bar{p}$) collider at Fermilab, 
%combined searches of the Higgs boson
%found a global excess of 2.5  standard deviation around 120~\gev, 
%using about 10.0~\ifb collision data~\cite{Group:2012zca}.
The searches for the SM Higgs boson in ATLAS and CMS cover all major Higgs decay channels,
including $H\to b\bar{b}$, $H\to W^+W^-$, $H\to \gamma\gamma$, 
$H\to ZZ$ and  $H\to \tau\tau$.
This thesis presents the observation of a SM Higgs boson in the Higgs to \zz to
 \llll decay channel (four-lepton analysis)
in $pp$ collisions at center-of-mass energies of 7 and 8~\tev\
 with the ATLAS detector at the LHC.

On July 4th 2012, the ATLAS and CMS collaboration independently announced the 
observation of a new particle using the $pp$ collision data at 
center-of-mass-energies of 7 and 8 \tev\ produced by the LHC~\cite{HIGG-2012-27, CMS-HIG-12-028}.
Three years later, by combing the ATLAS and CMS results, 
the mass of the discovered particle was precisely measured~\cite{HIGG-2014-14}:
$$125.09\;\pm\;0.21 (\text{stat})\;\pm\;0.11(\text{sys}) \gev$$
Once its mass is determined, other properties of the Higgs boson can be theoretically calculated and  
experimentally checked.
There has been great progress in understanding the main properties of the observed particle:
\begin{inparaenum}[(1)]%[\itshape a\ushape)]
\item its spin and charge-parity (CP) are tested against some other alternative assumptions, 
which have been excluded at more than 99.9\% confidence level using the ATLAS and CMS detector~\cite{HIGG-2013-17, CMS-HIG-12-041, CMS-HIG-13-001};
\item its CP invariance in the vector boson fusion (VBF) production is tested in di-tau decay channel and 
found to be consistent with the SM expection~\cite{Aad2016VBFdiTauSpinCP};
\item its production and decay rates are measured using the combined ATLAS and CMS analyses of the LHC \RunOne $pp$ data, the data are consistent with the SM predictions.~\cite{HIGG-2015-07}.
\end{inparaenum}
%Besides of these tests against theoretical predictions, generic measurements are also carried out
%in ATLAS and CMS via measuring the fiducial and differential cross section of the observed particle
%in di-photon, four-lepton and WW decay channels~\cite{PhysRevLett.115.091801, JHEPWWDiffXS, Khachatryan:2015yvw, Khachatryan:2016vnn, Khachatryan:2015rxa}.
%All the measurements are consistent with the predictions from SM within experimental uncertainties.

% the problem of SM and possible solut/searchesions.
%However, it is not excluded that the observed Higgs boson is a part of an extended symmetry structure 
%or that it emerges from a light resonance of a strongly coupled sector.
%This motivates the search for additional scalars as postulated by various extensions
%to the SM such as two Higgs doublet model (2HDM)~\cite{Lee:1973iz}.
This thesis also presents a search for additional heavy scalar resonances in the four-lepton final state,
using similar event selections as used in the SM Higgs boson observation.
The search uses 36.1~\ifb of $pp$ collision data collected 
with the ATLAS detector during 2015 and 2016 at a center-of-mass 
energy of 13~\tev.
It looks for a peak structure on top of the SM background in the four-lepton invariant mass spectrum, \mfl.
With a good mass resolution and high signal-to-background ratio, the \llll final state
is well suited to search for narrow resonances in all mass range.
Final results are interpreted as upper limits on the cross section times 
branching ratio for different signal hypotheses.
The first hypothesized signal is a heavy scalar particle under the narrow width approximation (NWA),
whose nature width is set to 4~\mev in  MC generation.
The heavy scalar is predominately produced by the gluon-fusion (ggF) 
and vector boson fusion (VBF) processes.
As various models also favor large width assumption (LWA), 
three benchmark models with the width of 1\%, 5\% and 10\% of resonance mass are studied. 
For large-width hypothesis, the interference between a heavy scalar and the SM Higgs boson 
as well as a heavy scalar and the $gg\to ZZ$ continuum background 
are taken into account.
The results for a NWA signal are combined with those in the \llvv final state to enhance the 
search sensitivities over the full mass range. 
%The combined upper limits are translated into the exclusion contours 
%in the context of the 2HDM. 
The results presented in this thesis extend previous searches published 
by ATLAS on the search for an additional heavy scalar
using 7 and 8~\tev\ $pp$ collision data~\cite{HIGG-2013-20}.
Similar results were also reported by CMS~\cite{CMS-HIG-13-031}.

This thesis is organized as following: 
Chapter~\ref{ch:sm_bsm} introduces a short overview of the Standard Model,
explains the Higgs mechanism, outlines  Higgs boson production and decay mechanisms
 and describes the 2HDM.
The LHC and ATLAS detector are described briefly in Chapter~\ref{ch:lhc} and~\ref{ch:atlas}. 
Collision data and simulated Monter Carlo (MC) data are summarized in 
Chapter~\ref{ch:data_mc},
followed by Chapter~\ref{ch:objects} on reconstruction of physics objects with the ATLAS detector.
Chapter~\ref{ch:sel} details event selection requirements 
and Chapter~\ref{ch:bkg} explains evaluation of the SM backgrounds using data-driven methods. 
 The modeling of signal and background is described in Chapter~\ref{ch:modeling}.
After the discussion of systematic uncertainties in Chapter~\ref{ch:sys}, 
final results and statistical interpretations are presented in Chapter~\ref{ch:results}.
Chapter~\ref{ch:comb} briefly introduces the \llvv analysis and then present the combined results.
Chapter~\ref{ch:conclusion} gives the conclusion and outlook.
