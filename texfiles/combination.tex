\clearpage
\section{Combination of the results from \llll and \llvv}
\label{ch:comb}

The \llll final state is featured by excellent mass resolution
while the \llvv benefits from the larger branching ratio.
Section~\ref{sec:llvv} provides an overview of the search 
for heavy resonances in the \llvv final state.
A statistical combination of \llll and \llvv final states leads 
to better search sensitivities over the whole mass range.
Section~\ref{sec:correlation} describes the correlation schemes used in the combination,
Section~\ref{sec:impact_uncertainty} discusses the impact of the uncertainties 
on the signal cross section and Section~\ref{sec:comb_res} shows the combined results.


\input{texfiles/llvv.tex}

\subsection{Correlation schemes}
\label{sec:correlation}
Same as the individual analysis, the combined results are also based on the 
simultaneous fit among
different signal regions defined separately in each analysis. 
To avoid double-counting, the orthogonality among signal regions is 
ensured when designing the analysis.
The systematic uncertainties, represented by nuisance parameters, 
are properly correlated. The experimental systematic uncertainties, evaluated by following 
recommendations from combined performance group, are fully correlated between the two analyses. 
The theoretical modeling uncertainties for \qqZZ and \ggZZ are also fully correlated.
The uncertainties resulting from data-driven methods are uncorrelated.
A given correlated uncertainty is modeled in the fit by using a
nuisance parameter common to all of the searches.

\subsection{Impact of the uncertainties on signal cross section}
\label{sec:impact_uncertainty}
The impact of a systematic uncertainty on the result depends on the production 
mode and the mass hypothesis. For the ggF production, at lower masses the 
luminosity uncertainty, the modeling uncertainty of the Z boson with 
associated jets background and the statistical uncertainty in $e\mu$ control 
region of the \llvv final state dominate, and at higher masses the 
uncertainties on the electron isolation efficiency become important,
as seen also in the VBF production. For the VBF production, the dominant
uncertainties come from the showering uncertainties and theoretical 
predictions of the ZZ events in the VBF category. Additionally at 
lower masses, the pileup reweighting and the jet energy resolution 
uncertainties are also important. Table~\ref{tab:ranking} shows the 
impact of the leading systematic uncertainties on the signal cross section,
which is set to the expected upper limit, for ggF production and VBF production.
The impact of the uncertainty from the integrated luminosity, 3.2\%, enters
both in the normalization of the fitted number of signal events as well as
in the background expectation from simulation. This leads to a luminosity uncertainty
which varies from 4\% to 7\% across the mass distribution, depending on 
the signal to background ratio.

\input{texfiles/impacts-table}

\subsection{Combination results}
\label{sec:comb_res}

%\subsection{Spin-0 resonance interpretation}
%\subsubsection{Results for a spin-0 resonance interpretation}
%\label{sec:resul}
%Limits from the combination of the two searches in the context of a spin-0 resonance 
%are described below. 

\paragraph{NWA interpretation}
%\paragraph{NWA results}
\label{sec:res_NWA}

Upper limits on the cross section times branching ratio ($\sigma \times\mbr(\htozz)$) for a 
heavy resonance are obtained as a
function of \mres\ with the $CL_{\textrm{s}}$ procedure~\cite{Read:2002hq} in the asymptotic approximation
from the combination of the two final states. It is assumed that an additional heavy scalar would be
produced predominantly via the ggF and VBF processes but that the ratio of the two production
mechanisms is unknown in the absence of a specific model. For this reason, fits for the ggF and VBF
production processes are done separately, and in each case the other process is allowed to float in
the fit as an additional nuisance parameter. Figure~\ref{fig:NWAlimits} presents the expected and
observed limits at 95\% confidence level on $\sigma \times\mbr(\htozz)$ of a narrow-width scalar for
the ggF (left) and VBF (right) production modes, as well as the expected limits from the $\llll$ and
$\llvv$ searches. This result is valid for models in which the width is less than 0.5\% of \mres.
When combining both final states, the 95\% CL upper limits range from 0.68~pb at $\mres = 242$~\gev\ 
to 11~fb at $\mres = 1200$~\gev\ for the gluon fusion production mode
and from 0.41~pb at $\mres = 236$~\gev\ to 13~fb at $\mres = 1200$~\gev\ 
for the vector boson fusion production mode.  
Compared with the results presented in \runa~\cite{HIGG-2013-20} where all four final states of \ZZ
decays were combined, the exclusion region presented here is significantly extended, depending
on the heavy scalar mass tested.

\begin{figure}[tbp]
  \subfigure[\label{fig:NWAlimits_sub1}]{\includegraphics[width=0.49\textwidth]{plot_res_NWA_ggF}}
  \subfigure[\label{fig:NWAlimits_sub2}]{\includegraphics[width=0.49\textwidth]{plot_res_NWA_VBF}}
  \caption{The upper limits at 95\% confidence level on  the cross section times branching 
    ratio for \subref{fig:NWAlimits_sub1} the ggF production mode
    ($\xsggF\times\mbr(\htozz)$) and \subref{fig:NWAlimits_sub2} for the VBF production 
    mode  ($\xsVBF\times\mbr(\htozz)$)  in the case of NWA\@. The green  and yellow  
    bands represent the $\pm1\sigma$ and $\pm2\sigma$ uncertainties on the expected limits. 
    The dashed coloured lines indicate the expected limits obtained from the 
    individual searches.
  \label{fig:NWAlimits}}
\end{figure}

%%%%%% LWA RESULTS %%%%%%
\paragraph{LWA interpretation}
%\paragraph{LWA results}
\label{sec:res_LWA}
%\textcolor{red}{LWA results and text to be updated with interference.} 
In the case of the LWA, limits on the cross section for the ggF production mode times branching ratio ($\sigma_{\textrm{ggF}} \times\mbr(\htozz)$) are set for different 
widths of the heavy scalar. The interference between the heavy scalar and the SM Higgs boson,
\IntHh, as well as the heavy scalar and the \ggZZ continuum, \IntHB, are modelled by either
analytical functions or reweighting the signal-only events as explained in
Sections~\ref{sec:h4l_sig_model} and~\ref{sec:h2l2v_sig_model}.  
Figures~\ref{fig:LWAlimits_1}, \ref{fig:LWAlimits_5}, and 
\ref{fig:LWAlimits_10} show the limits for a width of 1\%, 5\% and 10\% of
\mres respectively. The limits are set for masses of \mres\ higher than 400~\gev.

\begin{figure}[tbp]
  \centering
  \subfigure[\label{fig:LWAlimits_1}]{\includegraphics[width=0.45\textwidth]{plot_llvv_LWA_wH1}}
  \subfigure[\label{fig:LWAlimits_5}]{\includegraphics[width=0.45\textwidth]{plot_llvv_LWA_wH5}} 
  \subfigure[\label{fig:LWAlimits_10}]{\includegraphics[width=0.45\textwidth]{plot_llvv_LWA_wH10}}
  \caption {The 95\% confidence level limits on the cross section for the ggF 
    production mode times branching ratio ($\sigma_{\textrm{ggF}}\times\mbr(\htozz)$) 
    as function of \mres\ for an additional heavy scalar assuming a 
    width of~\subref{fig:LWAlimits_1} 1\%,~\subref{fig:LWAlimits_5} 5\%, 
    and~\subref{fig:LWAlimits_10} 10\% of \mres. 
    The green  and yellow  bands represent the $\pm1\sigma$ and 
    $\pm2\sigma$ uncertainties on the expected limits. 
    The dashed coloured lines indicate the expected limits obtained 
    from the individual searches. 
\label{fig:LWAlimits}}
\end{figure}
