%\clearpage
%\chapter{Statistical interpretation}
%\label{sec:interpretation}
\subsection{Statistical procedures} \label{sec:stats}
The statistical results are obtained from a simultaneous fit of \mfl in all categories,
under the framework of the RooFit~\cite{roofit:2003} and RooStats~\cite{roostats:2010}.
%using the general proedure for the LHC Higgs boson search~\cite{combReco:2011,comb7TeV:2012}, 
The compatibility of the observed data and background events as well as the upper limits on 
the cross section are evaluated by using the profiled likelihood ratio as the test statistics.
The profiled likelihood ratio $\Lambda(\alpha)$ is defined as:
\begin{equation}
    \label{eq:lambda}
    \Lambda(\alpha) = \frac{\mathsf{L}(\alpha, \hat{\hat{\vect{\theta}}}(\alpha))}{\mathsf{L}(\hat{\alpha}, \hat{\vect{\theta}})}
\end{equation}
where the $\alpha$ are the parameters of interest, $\vect{\theta}$ are the 
nuisance parameters that represent the systematic uncertainties and are 
constrained by Gaussian distributions.
The $\hat{\alpha}$ and $\hat{\vect{\theta}}$ refer to the unconditional 
maximum likelihood estimators
of the $\alpha$ and the $\vect{\theta}$, respectively, 
and the $\hat{\hat{\vect{\theta}}}(\alpha)$ refers to the best fitted 
values of $\vect{\theta}$ when
the parameters of interest are set to $\alpha$ as constant values.
So the profiled likelihood ratio is just the ratio of the likelihood from the fit with $\alpha$ being constant over
the one with $\alpha$ being free, and found to be the most powerful test accoridng to Neyman-Pearson lemma~\cite{10.2307/91247}. 

Construction of the likelihood depends on each analysis and the fit type.
In this analysis, an unbinned fit is used and the likelihood is constructed as the following:
\begin{equation}
    \label{eq:likelihood}
    \begin{split}
        \mathsf{L} &= \prod_i^{c}\;\mathsf{L}_i \\
        \mathsf{L}_i &= \text{Pois}(n_i|\sggF^i + \sVBF^i + B^i) \\
        &\cdot \left[ \prod\limits^{n_i}_{j=1}
  \frac{\sggF^i f_{\text{ggF}}(x_{j}) + \sVBF^i f_{\text{VBF}}(x_{j}) + B^i f_{\text{B}}(x_{j})}{\sggF^i + \sVBF^i + B^i}
        \right] \cdot \text{Gauss}(\vect{\theta};0,1) 
    \end{split}
\end{equation}
%where $\mathsf{L}_i$ is the likelihood for $i$th category,
The overall likelihood $\mathsf{L}$ is the multiplicative of the likelihood for each category $\mathsf{L}_i$,
each of which essentially includes three parts: 
\begin{inparaenum}[(1)]
\item the Poission term that accounts for the observed and expected number of events;
\item the likelihood functions that depend on the discriminating variables and are evaluated for each observed events;
\item the Gaussian constraint terms that represent auxilliary experiements that determine the size of each systematic uncertainties.
\end{inparaenum}
Explicitly $n_i$ is the total number of observed events in the signal region in $i$th category;
$\sggF^i, \sVBF^i\; \text{and}\; B^i$ are the expected number of events for ggF, VBF signal production and SM backgrounds 
in the singal region in $i$th category;
$f_X$ is the probability density function for each component, 
described in Section~\ref{sec:signal_modeling} for signal and Section~\ref{sec:bkg_model} for ZZ continuum background;
and $c$ is the total number of categories.
The expected number of events for each signal production, namely the normalization of the probability density function, 
is further parameterized as:
\begin{equation}
    \label{eq:exp_signal}
    S = \sigma \times \mathcal{BR} \times A \times C \times \int\mathcal{L} \times (1 + \epsilon(\vect{\theta}))
\end{equation}
where $\sigma$ is the total cross section;
$A\times C$ is the signal acceptance described in \refS{sec:signal_acc}; 
$\int\mathcal{L} = \lumiTot$ is the integrated luminosity of the dataset;
and the $\mathcal{BR}$ is the branching ratio of the heavy Higgs decays 
to the four lepton final state.
The term of $\epsilon(\vect{\theta})$ represents the impact of the systematic 
uncertainties $\vect{\theta}$.
These parameters implicitly have dependence on the mass \mH and width 
\widthH of the hypothetical resonance.
The probability distribution funciton of the profiled likelihood ratio is asymptotically 
described by the $\chi^2$ function with
the same degree of freedom as the one in the parameters of the interest~\cite{Cowan2011}.
Based on this statistical setup, the significance of an excess in the data is first quantified with the local $p_0$;
the probability that the background can produce a fluctuation greater than or equal to the excess observed in data.
The equivalent formulation in terms of number of standard deviation, $Z_{\text{local}}$, is referred to as the local significance.
%The global probability for the most significance excess to be observed anywhere else in a given search region is estimated 
%with the asymptotic formula described in Ref~\cite{Gross:2010qma}:
%\begin{equation}
%    \label{eq:global_p}
%    p_{\text{global}} = p_{\text{local}} \times (1 + \frac{\pi}{2} \times \mathcal{N} \times Z_{\text{local}})
%\end{equation}
%where $p_{\text{global}}$ is the global $p_0$, $\mathcal{N}$ is the number of observed upper crossing obtained from
%Figure~\ref{fig:p0}. The ratio of the global to the local probabiliteis ($p_0$), the trial factor used to correct 
%for the ``look elsewhere'' effect, increases with the range of hypothese considered, the mass resolutions and the 
%significance of the excess.
The exclusion limits on the cross section times branching ratio ($\sigma \times \mathcal{BR}$) are based on 
the $CL_s$ prescription~\cite{Read:2002hq};
a value of $\sigma \times \mathcal{BR}$ is regarded as excluded at 95\% confidence level when $CL_s$ is less than 5\%.

\section{$p_0$}
The statistical tests are performed in steps of values of the hypothesised resonance mass \mH.
\refF{fig:p0} shows the local $p_0$ as a function of $\mH$ for a scalar from ggF production under the NWA. 
Two excesses are observed for \mfl around 240 and 700~\gev, each with a local significance of 3.6~$\sigma$ estimated
under the asymptotic approximation, assuming the signal comes only from ggF production.
The local $p_0$ has non-trival dependence on the signal hypothesis. It is checked that in the case of 
LWA the local significance is lower than the one under NWA, indicating the two excesses are narrow width.
The global significance is evaluated from pseudo-experiments in the range of 
$200~\gev < \mH < 1200~\gev$ assuming the signal comes only from ggF production.
The pseduo-experiemnts are generated from background models by randomizing the global observables and the 
expected yields, and then evaluated in the same way as done for data to find the largest local
significance. Figure~\ref{fig:pdf_localP} shows the distribution of the maximum local significance of each
pseudo-experiment. Therefore given our search region it is expected to have a local excess with 
significance of about 2.4~$\sigma$. The probability of observing a local significance of 3.6~$\sigma$ is 
2.2~$\sigma$, which is the global significance that takes account the ``look-elsewhere'' effect.
\begin{figure}[!htbp]
\centering
\includegraphics[width=0.5\textwidth]{figures/p0/p0_categories_All}
\caption{$p_0$ as function of \mH in the NWA for exclusive category of $4\mu$, $4e$, $2\mu2e$, 
    VBF respectively and for these categories combined.  
\label{fig:p0}}
\end{figure}

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/p0/local_sigma_leading}
    \caption{The probability distribution function of local significance from pseudo-data.
    \label{fig:pdf_localP}
    }
\end{figure}

\subsection{Upper limits}
\paragraph{Narrow Width Approximation} \label{sec:resultsNWA}

Limits on the ggF and VBF cross-sections times branching ratio assuming 
the Narrow Width Approximation 
are obtained as a function of \mH with the $CL_{s}$ procedure in the asymptotic approximation, 
using the profiled likelihood ratio described in Section~\ref{sec:stats}. 
\refF{fig:NWAlimits_ggF} presents the expected and observed limits, at 95\% 
confidence level, on the cross section time branching ratio of the heavy Higgs
decaying to \llll final state in steps of comparable to the detector resolution.
Without a specfic model, the ratio of the ggF cross section and VBF cross section 
is unknown, therefore, when setting limits on the ggF production the VBF cross section
is profiled, and vise versa. 
This result is valid for models in which the width is less than 0.5\% of \mH.

\begin{figure}[!htbp]
\begin{center}
\includegraphics[width=0.49\textwidth]{figures/Limits/res_NWA_ggF}
\includegraphics[width=0.49\textwidth]{figures/Limits/res_NWA_VBF}
\end{center}
\caption{The upper limits at 95\% confidence level on then $\sigma_{ggF} \times BR(S\to ZZ\to 4\ell)$ (left) and $\sigma_{VBF} \times BR(S\to ZZ\to 4\ell)$ (right) under the NWA.}
\label{fig:NWAlimits_ggF}
\end{figure}

% ----------------------------------
% LWA Results
% 

\paragraph{Large Widths Assumption} \label{sec:resultsLWA}
In the case of LWA, limits on the cross section for the ggF production mode times branching
ratio ($\xsggF \times BR(H\to ZZ)$) are set for different widths of the heavy scalar.
The interference between the heavy scalar and the SM Higgs boson, \IntHh, as well as the 
heavy scalar and the \ggZZ continuum, \IntHB, are modeled by analytical functions as 
explained in Section~\ref{subsec:interference}.
The expected number of events for the heavy scalar and the interferences, based on 
leading-order QCD calculation, are proportional to and squred-root-of a signal strength modifier,
respecitively.
The signal strength modifiere is free in the fit.
Figure~\ref{fig:LWAlimits} shows the limits for a 
widths of 1\%, 5\% and 10\% of \mH, respectively. 
The limits are set for masses of \mH higher than 400~\gev.

\begin{figure}[!htbp]
\centering
    \subfigure[]{\includegraphics[width=0.46\textwidth]{figures/Limits/res_LWA_ggF_gamma0p01} \label{fig:lwa_1}}
    \subfigure[]{\includegraphics[width=0.46\textwidth]{figures/Limits/res_LWA_ggF_gamma0p05} \label{fig:lwa_5}}
    \subfigure[]{\includegraphics[width=0.46\textwidth]{figures/Limits/res_LWA_ggF_gamma0p1} \label{fig:lwa_10}}
    \caption{95\% confidence level limits on cross section for ggF production mode times
    the branching ratio ($\xsggF \times BR(H\to ZZ\to 4\ell)$) as function of 
    \mH for an additional heavy scalar assuming a width of 1\%~\subref{fig:lwa_1}, 
    5\%~\subref{fig:lwa_5} and 10\%~\subref{fig:lwa_10} of \mH. 
    The green and yellow bands represent the $\pm1\sigma$
    and $\pm2\sigma$ uncertainties on the expected limits.}
\label{fig:LWAlimits}
\end{figure}

\subsection{2HDM interpretation}
This section shows the interpretation of the results in the context of two-Higgs-doublet model (2HDM).
%While a short introduction to 2HDM is given in Chapter~\ref{ch:sm},
%the coupling of Higgs bosons and the SM particles is discussed in the following.

%The difference in the Higgs coupling to bottom quark accounts for the difference observed in 
%the exclusion limits shown in Figure~\ref{fig:2HDM_tanb_mH}.

Figure~\ref{fig:2HDM_tanb_cba_mH200} shows the exclusion limits in the \cosba versus \tanb plane
for Type-I and Type-II 2HDMs, for a heavy Higgs boson with mass of 200~\gev. 
This mass value is chosen so that the assumption of a narrow-width Higgs boson is valid over most
of the parameter space, and the experimental sensitivity is maximal.
The range of \cosba and \tanb presented is limited to the region where both the assumption of a 
heavy narrow-width Higgs boson and the purtabativity
in calculating the cross section are valid.
The upper limits at a given value of \cosba and \tanb is re-calculated by using the predicted 
ratio of ggF production rate over VBF.
\begin{figure}[!htbp]
\centering
\includegraphics[width=0.49\textwidth]{figures/Limits/exclusion-tanb-cba-mh200-typeI}
\includegraphics[width=0.49\textwidth]{figures/Limits/exclusion-tanb-cba-mh200-typeII}
\caption{The exclusion contour in the plane of $\tanb$ and $\cosba$ for $\mH = 200$~\gev\ 
for Type-I and Type-II.
The green and yellow bands represent the $\pm1\sigma$ and $\pm2\sigma$ uncertainties
on the expected limits. The hatched area shows the observed exclusion.
\label{fig:2HDM_tanb_cba_mH200}}
\end{figure}
Figure~\ref{fig:2HDM_tanb_mH} shows the exclusion limits in the \cosba 
versus \mH plane for $\cosba = -0.1$.
The valid range of \cosba is constrained by the measurement of the 
coupling of the SM Higgs boson with the 
Z-boson ($\kappa_h$), which is proportional to the $\sin(\beta - \alpha)$.
From the combined measurement of Higgs couplings at LHC~\cite{LHCcoupling2016}, 
the measured $\kappa_h$ 
is consistent with the SM prediction within the uncertainty of about 7\%, therefore the chosen 
\cosba is valid.
Comparing Figure~\ref{fig:2HDM_tanb_mH_1} and 
Figure~\ref{fig:2HDM_tanb_mH_2} in the region of
$\mH < 250~\gev$ and $\tanb > 1$, more parameter spaces are excluded in 
Type-I 2HDM than that in Type-II 2HDM\@.
% That is because the bottom quark contributes to the ggF production via the bottom quark loop.
As seen in Equation~\ref{eq:H_b_coupling_2} when $\tanb > 1$, 
the coupling modifier for the coupling of heavy Higgs to bottom quark gets larger than 1, 
leading to larger contribution in the total cross section 
 from the ggF production due to the bottom quark loop,
and larger branching ratio of the $b\bar{b}$ final state.
However when $\mH > 250~\gev$, the heavy Higgs boson is allowed to decay to two SM Higgs bosons.
That then becomes the dominant decay mode for a heavy Higgs boson, 
as shown in Figure~\ref{fig:2HDM_mH_br}. 
Figure~\ref{fig:2HDM_mH_diff} depicts the difference, between Type-I and Type-II, of 
the cross section of ggF production, 
branching ratio of the ZZ final state and the product of the two .
The difference in the cross section of ggF production resulting from the bottom quark loop
decreases when \mH increases.

\begin{figure}[!htbp]
\centering
    \subfigure[]{\includegraphics[width=0.47\textwidth]{figures/Limits/exclusion-tanb-mH-cba01-typeI} \label{fig:2HDM_tanb_mH_1}}
    \subfigure[]{\includegraphics[width=0.47\textwidth]{figures/Limits/exclusion-tanb-mH-cba01-typeII} \label{fig:2HDM_tanb_mH_2}}
\caption{The exclusion limits as function of \tanb and \mH with
    $\cos(\beta-\alpha) = -0.1$ for Type-I~\subref{fig:2HDM_tanb_mH_1} and Type-II~\subref{fig:2HDM_tanb_mH_2} 2HDM\@.
    The green and yellow bands represent the $\pm1\sigma$ and $\pm2\sigma$ uncertainties
on the expected limits. The hatched area shows the observed exclusion.
\label{fig:2HDM_tanb_mH}}
\end{figure}
\begin{figure}[!htbp]
    \centering
    \includegraphics[width=0.49\textwidth]{figures/fig_mH_br_1}
    \includegraphics[width=0.49\textwidth]{figures/fig_mH_br_2}
    \caption{The branching ratio of each heavy Higgs decay mode 
    as a function of \mH for Type-I and Type-II 2HDM\@. 
    The chosen parameters are: $\tanb = 3.6$, $\sinba = 0.994987$, $\cosba = 0.1$ and $m_h = 125~\gev$.
    \label{fig:2HDM_mH_br}}
\end{figure}
\begin{figure}[!htbp]
    \centering
    \includegraphics[width=0.49\textwidth]{figures/fig_mH_diff_xs}
    \caption{The difference in cross section of ggF production ($\sigma$),
    branching ratio of ZZ final state $BR$, and the cross section times branching ratio ($\sigma\times BR$) 
    between Type-I and Type-II.
    \label{fig:2HDM_mH_diff}}
\end{figure}

The hatched red area in this exclusion plots is the excluded parameter space.
Compared with the results prestented in Run 1~\cite{Aad:2015kna},
the exclusion limits presented here is almost twice more stringent.
