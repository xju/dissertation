% Z + mumu background
\section{\llmm background}

Multiple control regions are defined to target different sources of fake backgrounds:
% label={\alph*)}, font={\color{red!50!black}}
\begin{enumerate}
    \item \textbf{Inverted $d_0$ significance CR:}
        The standard selections are applied on the leading di-lepton except 
        the vertex requireement. No isolation criteria are applied on the subleading 
        di-muon pair, which instead has at least one lepton failed the $d_0$ 
        significance selection. This control region is enhanced in Z+HF 
        and \ttbar, since the leptons from heavy-flavor hadrons are characterized 
        by large $d_0$ significance. It will be used to extract the number of events
        for Z+HF and \ttbar by fitting together with the $e\mu + \mu\mu$ CR simultaneously.

    \item \textbf{$e\mu + \mu\mu$ CR:} %\linebreak
        In this control region an opposite-charge different-flavor leading 
        di-lepton is required, and it must pass the standard four-lepton analysis selections.
        In this way the leading lepton pair cannot be originated from the Z boson decay, 
        guaranteeing a clean \ttbar CR\@. The vertex cut is not applied on the quadruplet 
        to gain statistics. The $d_0$ significance is not applied to the subleading dilepton 
        pair, nor are the isolation and leptons' charge criteria.

    \item \textbf{Relaxed CR:} %\linebreak
        It contains all types of backgrounds with relatively large statistics.
        The standard four-lepton analysis selection is applied on the quadruplet,
        but $d_0$ and isolation are not applied on the subleading lepton pair, 
        nor is the vertex requirement. Each background component in the CR is 
        estimated from the other control regions, and then extrapolated to signal region.

    \item \textbf{Inverted isolation CR:} %\linebreak
        The standard four-lepton analysis selection is applied on the leading dilepton.
        The subleading dilepton pair is required to pass the $d_0$ significance selection 
        but has at least one lepton fail the isolation criteria.
        The vertex cut is applied to reject the contamination from Z+HF and \ttbar.
        This CR is used for the estimate of Z+LF after the two other components are obtained. 
\end{enumerate}

% the distribution of m12 from CRs.
The invariant mass of the leading dilepton pair ($m_{12}$) separates the Z+HF from the 
\ttbar as it is a peak for Z+HF but a non-resonant distribution for \ttbar, 
as shown in Figure~\ref{fig:CR_DataMC}. The modeling of $m_{12}$ is taken from MC 
simulation, and parameterized by 2nd order Chebyshev polynominal for \ttbar events and
by a Breit-Wigner function convolved with a Crystal Ball function for Z$+$jets events.
% m12 modeling
However, the $m_{12}$ of Z+jets events in the $e\mu+\mu\mu$ CR does not form
a peak but a flat spectrum because the leading dilepton are likely from random 
opposite flavor leptons, therefore is modeled by a first order polynominal.

\begin{figure}[htb]
  \centering
  \subfigure[inverted $d_0$ significance CR]{\includegraphics[width=0.45\linewidth]{figures/reducible_bkg/CRA_DATAandMC}}
  \subfigure[$e\mu+\mu\mu$ CR]{\includegraphics[width=0.45\linewidth]{figures/reducible_bkg/CRC_DATAandMC}}
  \caption{The pre-fit $m_{12}$ distributions for data and MC-simulated
    events for different processes for the control regions enhanced
    in $Z$+heavy-flavour jets (left) and $t\bar{t}$ (right).\label{fig:CR_DataMC}
  }
\end{figure}

% normalization of the m12 for each CR.
The normalization of Z+HF and \ttbar in each of the two fitted CRs is expressed 
as a function of the number of events for each component in the relaxed CR\@. 
The extended probability distribution function in the simultaneous fit is defined as:
\begin{equation}
\mathcal{F}_{\text{CR}} = \sum_i\: N_i \cdot f_i \cdot M_i,
\end{equation} 
where $i$ is an index running for all contributions (Z+HF, \ttbar, WZ, \zz);
$f_i$, obtained from MC simulation, is the ratio 
of the yields in the given control region over the yields in the 
relaxed CR; $M_i$ is the probability distribution function of the $m_{12}$ of 
each contribution.
Figure~\ref{fig:muFits} shows the post-fit spectrum of the $m_{12}$ in each CR\@.

\begin{figure}[!htb]
 \centering
    \subfigure[\label{fig:muFitInvD0}]{\includegraphics[width=0.49\linewidth]{figures/reducible_bkg/Fit_CRA_data}}
    \subfigure[\label{fig:muFitEmu}]{\includegraphics[width=0.49\linewidth]{figures/reducible_bkg/Fit_CRC_data}}\\
    \caption{Distributions of $m_{12}$ for data (points with error
      bars) in two control regions. The data are fitted
      simultaneously in the the inverted $d_0$ (a) and the
      $e\mu$+$\mu\mu$ (b) CRs and the result of the fit is shown with
      the continuous line. The dashed lines correspond to the
      $Z$+HF and $\ttbar$ components of the fit. The $\chi^2/\text{NDoF}$ is 21.63/24 = 0.90.
      \label{fig:muFits}}
\end{figure}

The Z+LF contribution in the relaxed CR is estimated from the inverted isolation 
control region. This CR has a significant amount of contributions from Z+LF background 
but is also contaminated with other backgrounds, as seen in Figure~\ref{fig:CRBDATAMC}.
Again the \zz and WZ contribution is estimated from MC simulation,
but the Z+HF and \ttbar are rescaled from the previous data driven estimate.
The post-fitted $m_{12}$ spectrum suggests there is no LF background, as shown 
in Figure~\ref{fig:CRBfit}.

\begin{figure}[!htb]
 \centering
    \subfigure[\label{fig:CRBDATAMC}]{\includegraphics[width=0.49\linewidth]{figures/reducible_bkg/CRB_DATAandMC.eps}}
    \subfigure[\label{fig:CRBfit}]{\includegraphics[width=0.49\linewidth]{figures/reducible_bkg/Fit_CRB_data.eps}}\\
    \caption{(a) The $m_{12}$ distributions for data and MC-simulated events for different 
    processes for the inverted isolation control region. (b) Post-fit $m_{12}$ distribution 
    in this CR shows no Z+LF contribution. The $\chi^2/\text{NDoF}$ is 136.53/12 = 1.14.}
\end{figure}

Finally the estimated number of events for each contribution in the relaxed CR 
is extrapolated to the SR using the MC-based transfer factor, which is defined as 
the ratio of the SR yields over the relaxed CR yields. 
The transfer factor and the estimated number of events for each contribution 
are summarized in Table~\ref{tab:muFits}.
The fit is repeated separately in the $4\mu$ and $2e2\mu$ channels to obtain the yield in 
each channel: the $4\mu$ channel is 55\% of the total and the $2e2\mu$ channel 45\%.

\begin{table}[!htb]
  \centering
  \caption{Data fit results for the $Z$+HF, $\ttbar$ and $Z$+LF yields
    in the relaxed control region, shown together with the
    per-event efficiencies from MC simulation. The errors on the fit results and 
    transfer factors are statistical only.
    The final estimate in the signal region is also shown with its
    statistical and systematic uncertainty. The statistical uncertainty for the 
    $Z$+LF signal yield is not shown since this one is 100\% correlated with that of 
    the $Z$+HF\@. The $WZ$ contribution is also shown.
    %The contribution of $WZ$ in the signal region is a MC-based estimation and is also shown with its large uncertainty, coming from the statistics of the MC sample.
    \label{tab:muFits}}
  \vspace{0.2cm}
  \begin{tabular}{cccc}
    \hline
    \hline
    $4\mu$+$2e2\mu$\\
    \hline
    type & data fit & extrapolation factor [\%] & SR yield \\
    \hline
    $Z$+jets (HF)  & $928 \pm 50$ & $0.75 \pm 0.09$ & $6.96 \pm 0.37 \pm 1.19 $ \\
    $\ttbar$       & $934 \pm 23$ & $0.25 \pm 0.03$ & $2.33 \pm 0.06 \pm 0.44 $ \\
    \hline
        $Z$+jets (LF)  &  $0 \pm 16$ &  $3.00 \pm 0.40$  & $0 \pm 0.49 \pm 0.25$ \\
  \hline
 $WZ$ & \multicolumn{2}{c}{(MC-based estimation)} & $0.91 \pm 0.50 $ \\
    \hline
    \hline
  \end{tabular}
\end{table}
