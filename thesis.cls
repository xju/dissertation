%*********************************************************************
%
% thesis.cls
%
% Astronomy thesis DOCUMENT STYLE for LaTeX2e
%
% Created: 1991: Siebers
%
% Modification History - 
%
%    July 1996, Eric Jensen -
%	Totally overhauled the style.  It was based on the Latex
%	``book'' style (and still is).  Before the whole file was
%	basically a copy of the Latex 2.09 book.sty file, with a few
%	things changed here and there.  Now we load the Latex2e book
%	class at the beginning, and then change only those things which
%	need to be changed (which turns out to be not very much).  A
%	modified version of the AASTeX style is loaded which defines
%	all of the commands and environments used there.
%
%	The ``thesis'' style now can be invoked with various options,
%	including type sizes (10pt, 11pt, 12pt), and with various
%	margins: ``draft'' gives smaller margins and line spacing,
%	suitable for handing to your committee but without using a
%	whole ream of paper; ``final'' conforms to the grad school
%	margin requirements and is suitable for the final copy.
%	Default is \documentclass[12pt,final]{thesis}.
%
%    July 1996, Andrea Cox:
%	Added a \fixspacing command you can use if you want to guarantee
%	  single-spaced figure and table captions in your final draft.
%	  Insert this command within the caption environments (see below).
%	Redefined the table spacing within the \draftsize and \finalsize
%	  commands, to ensure single-spacing of the table rows.  (See the
%	  "arraystretch" option.)
%
%*********************************************************************

\ProvidesClass{thesis}[1996/07/09 v1.0 UW Astronomy thesis document class]

\newcommand\@myptsize{12}	% Default point size is 12.

\newcommand\singlespace{\renewcommand\baselinestretch{1.2}}
\newcommand\doublespace{\renewcommand\baselinestretch{1.8}}
\newcommand\fixspacing{\setlength{\baselineskip}{1.2em}}
	% Insert this command in your table and figure captions if you
	% want them to be single-spaced in the final version.
%\newcommand\fixtabspacing{\renewcommand\arraystretch{0.2}}

\newcommand\draftsize{
  \setlength\overfullrule{5pt}
  \setlength{\textwidth}{6.5in}
  \setlength{\oddsidemargin}{0in}
  \setlength{\textheight}{8.7in}
  \setlength{\topmargin}{0.in}
  \setlength{\parindent}{0.4in}
  \setlength{\parskip}{0.1in}
  \renewcommand\baselinestretch{1.2}
  \renewcommand\arraystretch{0.9}	% Sets the row spacing of the tables
					%   to 0.9 times the "baselinestretch"
  }

\newcommand\finalsize{
  \setlength\overfullrule{0pt}
  \setlength{\textwidth}{6.05in}
  \setlength{\oddsidemargin}{0.35in}
  \setlength{\textheight}{8.5in}
  \setlength{\topmargin}{0.05in}
  \setlength{\headsep}{0.225in}
  \setlength{\parindent}{0.4in}
  \renewcommand\baselinestretch{1.8}
  \renewcommand\arraystretch{0.6}	% Sets the row spacing of the tables
					%   to 0.6 times the "baselinestretch"
  }


\DeclareOption{10pt}{\renewcommand\@myptsize{10}}
\DeclareOption{11pt}{\renewcommand\@myptsize{11}}
\DeclareOption{12pt}{\renewcommand\@myptsize{12}}

% The next two options, ``draft'' and ``final'' determine how big the
% margins are.  ``Final'' is the default.  Their processing is deferred
% to the end of the class so that the ``book'' class below doesn't step
% on the definitions.

% Use the draft option for printing to save paper when you don't have
% to generate a copy that meets grad school requirements.
\DeclareOption{draft}{\AtEndOfClass{\draftsize}}

% The ``final'' option will pass the Margin Lady.
\DeclareOption{final}{\AtEndOfClass{\finalsize}}

% If they don't specify "draft" or "final", use final for the default:
\ExecuteOptions{final}

\ProcessOptions

% Construct a point-size option name (e.g. ``12pt'') based on the
% options specified above:

\def\ptoption{\@myptsize pt}

% Load the Latex2e ``book'' class to get the basic definitions:
\LoadClass[oneside,\ptoption]{book}

% Also input a modified version of the AAS MS style to add various
% symbol definitions:
%\input{thesis_aasms.sty}


% Fix the "chapter" command so it doesn't change the page style for
% the first page of each chapter (took out "\thispagestyle{plain}"

\renewcommand\chapter{
	\if@openright\cleardoublepage\else\clearpage\fi
	\global\@topnum\z@
	\@afterindentfalse
	\secdef\@chapter\@schapter
	}

% Slight redefinition of two commands which change the size of the chapter
% headings.  If you want different sizes, change the two commands below.

% This controls the size of chapter titles.
\newcommand\chaptertitlesize{\LARGE}

% This controls the size of the heading for table of contents, list
% of figures, and list of tables.
\newcommand\toctitlesize{\LARGE}

% Here's where those sizes get implemented:
\def\@makechapterhead#1{%
  \vspace*{50\p@}%
  {\parindent \z@ \raggedright \normalfont
    \ifnum \c@secnumdepth >\m@ne
      \if@mainmatter
        \chaptertitlesize\bfseries \@chapapp{} \thechapter
        \par\nobreak
        \vskip 20\p@
      \fi
    \fi
    \interlinepenalty\@M
    \chaptertitlesize \bfseries #1\par\nobreak
    \vskip 40\p@
  }}

\def\@makeschapterhead#1{%
  \vspace*{50\p@}%
  {\parindent \z@ \raggedright
    \normalfont
    \interlinepenalty\@M
    \toctitlesize \bfseries  #1\par\nobreak
    \vskip 40\p@
  }}

% Changed the definitions of section, subsection, and subsubsection 
% here to allow more control of the spacing around them.

\newlength{\beforesecskip}
\newlength{\aftersecskip}
\beforesecskip = 2.ex		% Originally -1.ex
\aftersecskip = 1.ex		% Originally -2.ex
\def\section#1{
\vspace*{\beforesecskip}
\@startsection {section}{1}{\z@}
{0ex}{0.001ex}{\large\bf}{#1}
\vspace*{\aftersecskip}}
\def\subsection#1{\vspace*{\beforesecskip}
\@startsection{subsection}{2}{\z@}
{0ex}{0.001ex}{\normalsize\bf}{#1}\vspace*{\aftersecskip}}
\def\subsubsection#1{\vspace*{\beforesecskip}
\@startsection{subsubsection}{3}{\z@}
{0ex}{0.001ex}{\normalsize\bf}{#1}\vspace*{\aftersecskip}}

% Also give the user the opportunity to set the page footer and header
% from within the document.  By default, the header does the page
% numbering, but you might want to put extra info in the footers.  One
% example: \setfoot{\tiny \hss DRAFT \today\hss} will create a footer
% with the word "DRAFT" and today's date in small print centered at the
% bottom of each page - handy when dealing with multiple drafts and
% trying to remember what you printed when.

\def\setfoot#1{\def\@evenfoot{#1}\let\@oddfoot=\@evenfoot}
\def\sethead#1{\let\savehead=\@evenhead\def\@evenhead{#1}
           \let\@oddhead=\@evenhead\let\oldhead=\savehead}

% Appendix for an individual chapter; reset the section and equation
% counters, saving the old values to be restored with the
% \endchapappendix command.

\def\chapappendix{\par
\setcounter{section}{0}
\setcounter{subsection}{0}
\setcounter{equation}{0}
\let\savesection=\thesection
\let\saveequation=\theequation
\def\thesection{\Alph{section}}
\def\theequation{\hbox{\normalsize\Alph{section}\arabic{equation}}}
}

% End of chapappendix def.
\def\endchapappendix{\let\thesection=\savesection
  \let\theequation=\saveequation
}

% If you want a footnote with no marker, just the footnote text at the
% bottom of the page, use \blanknote{text}

\long\def\@makeblankfntext#1{\parindent 1em\noindent
            \hbox to 1.8em{\hss$^{}$}#1}

\def\blanknote#1{\let\savefn=\@makefntext
\let\@makefntext=\@makeblankfntext
\footnotetext{#1}\let\@makefntext=\savefn}

% Allow hyphenation:
   \pretolerance=100
   \tolerance=200

% Change tocdepth and secnumdepth; these control which level of
% sectioning will be numbered in the text and will be included in the
% table of contents; default is 2, which is subsection for this style;
% change to 3 to include subsubsections:

\setcounter{tocdepth}{3}
\setcounter{secnumdepth}{3}



%    ****************************************
%    *             FOOTNOTES                *
%    ****************************************
%
% \footnoterule is a macro to draw the rule separating the footnotes from
% the text.  It should take zero vertical space, so it needs a negative
% skip to compensate for any positive space taken by the rule.  (See
% PLAIN.TEX.)

% Changed 7/1/96 allow a little more space around footnotes; the
% default vertical spacing is too crowded for my tastes; ELNJ
\renewcommand\footnoterule{%
  \hrule\@width.4\columnwidth
  \kern8.6\p@}


% Not sure if these abstract definitions are actually useful, but
% it doesn't hurt to leave them here...

\def\abstractpage{\@restonecolfalse\if@twocolumn\@restonecoltrue\onecolumn
     \else \newpage \fi \thispagestyle{plain}}

\def\endabstractpage{\if@restonecol\twocolumn \else \newpage \fi}

\def\abstract{\abstractpage
\addcontentsline{toc}{chapter}{\protect
\numberline{\ }Abstract}
\null\vfil
\begin{center}
{\bf Abstract}
\end{center}}

\def\endabstract{\par\vfil\null\endabstractpage}

%    ****************************************
%    *            PAGE STYLES               *
%    ****************************************
%
% The page style 'foo' is defined by defining the command \ps@foo.  This
% command should make only local definitions.  There should be no stray
% spaces in the definition, since they could lead to mysterious extra
% spaces in the output.
%
% The \ps@... command defines the macros \@oddhead, \@oddfoot,
% \@evenhead, and \@evenfoot to define the running heads and
% feet---e.g., \@oddhead is the macro to produce the contents of the
% heading box for odd-numbered pages.  It is called inside an \hbox of
% width \textwidth.
%
% To make headings determined by the sectioning commands, the page style
% defines the commands \chaptermark, \sectionmark, ... , where
% \chaptermark{TEXT} is called by \chapter to set a mark, and so on.
% The \...mark commands and the \...head macros are defined with the
% help of the following macros.  (All the \...mark commands should be
% initialized to no-ops.)
%
% MARKING CONVENTIONS:
% LaTeX extends TeX's \mark facility by producing two kinds of marks
% a 'left' and a 'right' mark, using the following commands:
%     \markboth{LEFT}{RIGHT} : Adds both marks.
%     \markright{RIGHT}      : Adds a 'right' mark.
%     \leftmark  : Used in the \@oddhead, \@oddfoot, \@evenhead or \@evenfoot
%                  macro, gets the current 'left'  mark.  Works like TeX's
%                  \botmark command.
%     \rightmark : Used in the \@oddhead, \@oddfoot, \@evenhead or \@evenfoot
%                  macro, gets the current 'right'  mark.  Works like TeX's
%                  \firstmark command.
% The marking commands work reasonably well for right marks 'numbered
% within' left marks--e.g., the left mark is changed by a \chapter command and
% the right mark is changed by a \section command.  However, it does
% produce somewhat anomalous results if two \bothmark's occur on the same page.
%
% Commands like \tableofcontents that should set the marks in some page styles
% use a \@mkboth command, which is \let by the pagestyle command (\ps@...)
% to \markboth for setting the heading or \@gobbletwo to do nothing.

\mark{{}{}}   % Initializes TeX's marks

%    ****************************************
%    *     THESIS PAGE STYLE                *
%    ****************************************
%
\def\ps@thesis{\let\@mkboth\@gobbletwo%
    \def\@oddfoot{}\def\@evenfoot{}%
    \def\@oddhead{\rm\hfil\thepage}%
    \def\@evenhead{\rm\thepage\hfil}}

%    ****************************************
%    *           INITIALIZATION             *
%    ****************************************
%
% Default initializations

\ps@thesis                  % 'thesis' page style
\pagenumbering{arabic}      % Arabic page numbers
\onecolumn                  % Single-column.
\if@twoside\else\raggedbottom\fi % Ragged bottom unless twoside option.
