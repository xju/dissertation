#!/bin/bash 

#mv limit_input_NWA_ggF.txt_data.pdf limit_ggF_NW.pdf 
#mv limit_input_NWA_VBF.txt_data.pdf limit_VBF_NW.pdf 
#mv limit_input_LWA_ggF_0.0001.txt_data.pdf limit_XS_ggF_LWA_NWA.pdf
#mv limit_input_LWA_ggF_0.01.txt_data.pdf limit_XS_ggF_LWA1.pdf
#mv limit_input_LWA_ggF_0.05.txt_data.pdf limit_XS_ggF_LWA5.pdf 
#mv limit_input_LWA_ggF_0.1.txt_data.pdf limit_XS_ggF_LWA10.pdf 

#mv limit_input_limit_NWA_ggF.txt_data.eps limit_ggF_NW.eps 
#mv limit_input_limit_NWA_VBF.txt_data.eps limit_VBF_NW.eps 
#mv limit_input_LWA_ggF_0.0001.txt_data.eps limit_XS_ggF_LWA001.eps
#mv limit_input_LWA_ggF_0.01.txt_data.eps limit_XS_ggF_LWA1.eps
#mv limit_input_LWA_ggF_0.05.txt_data.eps limit_XS_ggF_LWA5.eps 
#mv limit_input_LWA_ggF_0.1.txt_data.eps limit_XS_ggF_LWA10.eps 

mv input_NWA_ggF.eps limit_ggF_NW.eps 
mv input_NWA_VBF.eps limit_VBF_NW.eps 
mv input_LWA_ggF_0.01.eps limit_XS_ggF_LWA1.eps
mv input_LWA_ggF_0.05.eps limit_XS_ggF_LWA5.eps 
mv input_LWA_ggF_0.1.eps limit_XS_ggF_LWA10.eps 
